Rails.application.routes.draw do

  mount_devise_token_auth_for 'User', at: 'auth'
  match '/auth/confirm_registration', to: 'application#confirm_registration', via: :get

  resources :resumes do
    collection do
      get :resume_template_details
    end
  end
  resources :basic_info
  resources :contact_info
  resources :character_references
  resources :educations do
    member do
      get :attachments
      post :upload_attachment
      post :destroy_attachment
    end
  end
  resources :qualifications do
    member do
      get :attachments
      post :upload_attachment
      post :destroy_attachment
    end
  end
  resources :photos
  resources :skills
  resources :work_experiences do
    member do
      get :attachments
      post :upload_attachment
      post :destroy_attachment
    end
  end
  resources :newsletter_subscribers
  resources :previews do
    member do
      get :debug_params
      get :generate_css, as: 'generate_css'
      get :cover_letter
    end
  end
  resources :resume_configs do
    collection do
      get :fonts
    end
  end
  resources :dashboards
  resources :resume_page_setups
  resources :cover_letters
  resources :video_pitches

  resources :users do
    collection do
      get :myprofile
    end
  end

  root 'frontend/home#coming_soon'
  match 'homepage', to: 'frontend/home#index', via: :get, as: 'homepage'
  match 'login', to: 'frontend/home#auth', via: :get, as: 'login'
  match 'signup', to: 'frontend/home#signup', via: :get, as: 'signup'
  match 'auth/set_session', to: 'api#set_session', via: :post

end
