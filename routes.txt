                            Prefix Verb     URI Pattern                                        Controller#Action
                  new_user_session GET      /auth/sign_in(.:format)                            devise_token_auth/sessions#new
                      user_session POST     /auth/sign_in(.:format)                            devise_token_auth/sessions#create
              destroy_user_session DELETE   /auth/sign_out(.:format)                           devise_token_auth/sessions#destroy
                 new_user_password GET      /auth/password/new(.:format)                       devise_token_auth/passwords#new
                edit_user_password GET      /auth/password/edit(.:format)                      devise_token_auth/passwords#edit
                     user_password PATCH    /auth/password(.:format)                           devise_token_auth/passwords#update
                                   PUT      /auth/password(.:format)                           devise_token_auth/passwords#update
                                   POST     /auth/password(.:format)                           devise_token_auth/passwords#create
          cancel_user_registration GET      /auth/cancel(.:format)                             devise_token_auth/registrations#cancel
             new_user_registration GET      /auth/sign_up(.:format)                            devise_token_auth/registrations#new
            edit_user_registration GET      /auth/edit(.:format)                               devise_token_auth/registrations#edit
                 user_registration PATCH    /auth(.:format)                                    devise_token_auth/registrations#update
                                   PUT      /auth(.:format)                                    devise_token_auth/registrations#update
                                   DELETE   /auth(.:format)                                    devise_token_auth/registrations#destroy
                                   POST     /auth(.:format)                                    devise_token_auth/registrations#create
               auth_validate_token GET      /auth/validate_token(.:format)                     devise_token_auth/token_validations#validate_token
                      auth_failure GET      /auth/failure(.:format)                            devise_token_auth/omniauth_callbacks#omniauth_failure
                                   GET      /auth/:provider/callback(.:format)                 devise_token_auth/omniauth_callbacks#omniauth_success
                                   GET|POST /omniauth/:provider/callback(.:format)             devise_token_auth/omniauth_callbacks#redirect_callbacks
                  omniauth_failure GET|POST /omniauth/failure(.:format)                        devise_token_auth/omniauth_callbacks#omniauth_failure
                                   GET      /auth/:provider(.:format)                          redirect(301)
         auth_confirm_registration GET      /auth/confirm_registration(.:format)               application#confirm_registration
   resume_template_details_resumes GET      /resumes/resume_template_details(.:format)         resumes#resume_template_details
                           resumes GET      /resumes(.:format)                                 resumes#index
                                   POST     /resumes(.:format)                                 resumes#create
                        new_resume GET      /resumes/new(.:format)                             resumes#new
                       edit_resume GET      /resumes/:id/edit(.:format)                        resumes#edit
                            resume GET      /resumes/:id(.:format)                             resumes#show
                                   PATCH    /resumes/:id(.:format)                             resumes#update
                                   PUT      /resumes/:id(.:format)                             resumes#update
                                   DELETE   /resumes/:id(.:format)                             resumes#destroy
                  basic_info_index GET      /basic_info(.:format)                              basic_info#index
                                   POST     /basic_info(.:format)                              basic_info#create
                    new_basic_info GET      /basic_info/new(.:format)                          basic_info#new
                   edit_basic_info GET      /basic_info/:id/edit(.:format)                     basic_info#edit
                        basic_info GET      /basic_info/:id(.:format)                          basic_info#show
                                   PATCH    /basic_info/:id(.:format)                          basic_info#update
                                   PUT      /basic_info/:id(.:format)                          basic_info#update
                                   DELETE   /basic_info/:id(.:format)                          basic_info#destroy
                contact_info_index GET      /contact_info(.:format)                            contact_info#index
                                   POST     /contact_info(.:format)                            contact_info#create
                  new_contact_info GET      /contact_info/new(.:format)                        contact_info#new
                 edit_contact_info GET      /contact_info/:id/edit(.:format)                   contact_info#edit
                      contact_info GET      /contact_info/:id(.:format)                        contact_info#show
                                   PATCH    /contact_info/:id(.:format)                        contact_info#update
                                   PUT      /contact_info/:id(.:format)                        contact_info#update
                                   DELETE   /contact_info/:id(.:format)                        contact_info#destroy
              character_references GET      /character_references(.:format)                    character_references#index
                                   POST     /character_references(.:format)                    character_references#create
           new_character_reference GET      /character_references/new(.:format)                character_references#new
          edit_character_reference GET      /character_references/:id/edit(.:format)           character_references#edit
               character_reference GET      /character_references/:id(.:format)                character_references#show
                                   PATCH    /character_references/:id(.:format)                character_references#update
                                   PUT      /character_references/:id(.:format)                character_references#update
                                   DELETE   /character_references/:id(.:format)                character_references#destroy
             attachments_education GET      /educations/:id/attachments(.:format)              educations#attachments
       upload_attachment_education POST     /educations/:id/upload_attachment(.:format)        educations#upload_attachment
      destroy_attachment_education POST     /educations/:id/destroy_attachment(.:format)       educations#destroy_attachment
                        educations GET      /educations(.:format)                              educations#index
                                   POST     /educations(.:format)                              educations#create
                     new_education GET      /educations/new(.:format)                          educations#new
                    edit_education GET      /educations/:id/edit(.:format)                     educations#edit
                         education GET      /educations/:id(.:format)                          educations#show
                                   PATCH    /educations/:id(.:format)                          educations#update
                                   PUT      /educations/:id(.:format)                          educations#update
                                   DELETE   /educations/:id(.:format)                          educations#destroy
         attachments_qualification GET      /qualifications/:id/attachments(.:format)          qualifications#attachments
   upload_attachment_qualification POST     /qualifications/:id/upload_attachment(.:format)    qualifications#upload_attachment
  destroy_attachment_qualification POST     /qualifications/:id/destroy_attachment(.:format)   qualifications#destroy_attachment
                    qualifications GET      /qualifications(.:format)                          qualifications#index
                                   POST     /qualifications(.:format)                          qualifications#create
                 new_qualification GET      /qualifications/new(.:format)                      qualifications#new
                edit_qualification GET      /qualifications/:id/edit(.:format)                 qualifications#edit
                     qualification GET      /qualifications/:id(.:format)                      qualifications#show
                                   PATCH    /qualifications/:id(.:format)                      qualifications#update
                                   PUT      /qualifications/:id(.:format)                      qualifications#update
                                   DELETE   /qualifications/:id(.:format)                      qualifications#destroy
                            photos GET      /photos(.:format)                                  photos#index
                                   POST     /photos(.:format)                                  photos#create
                         new_photo GET      /photos/new(.:format)                              photos#new
                        edit_photo GET      /photos/:id/edit(.:format)                         photos#edit
                             photo GET      /photos/:id(.:format)                              photos#show
                                   PATCH    /photos/:id(.:format)                              photos#update
                                   PUT      /photos/:id(.:format)                              photos#update
                                   DELETE   /photos/:id(.:format)                              photos#destroy
                            skills GET      /skills(.:format)                                  skills#index
                                   POST     /skills(.:format)                                  skills#create
                         new_skill GET      /skills/new(.:format)                              skills#new
                        edit_skill GET      /skills/:id/edit(.:format)                         skills#edit
                             skill GET      /skills/:id(.:format)                              skills#show
                                   PATCH    /skills/:id(.:format)                              skills#update
                                   PUT      /skills/:id(.:format)                              skills#update
                                   DELETE   /skills/:id(.:format)                              skills#destroy
       attachments_work_experience GET      /work_experiences/:id/attachments(.:format)        work_experiences#attachments
 upload_attachment_work_experience POST     /work_experiences/:id/upload_attachment(.:format)  work_experiences#upload_attachment
destroy_attachment_work_experience POST     /work_experiences/:id/destroy_attachment(.:format) work_experiences#destroy_attachment
                  work_experiences GET      /work_experiences(.:format)                        work_experiences#index
                                   POST     /work_experiences(.:format)                        work_experiences#create
               new_work_experience GET      /work_experiences/new(.:format)                    work_experiences#new
              edit_work_experience GET      /work_experiences/:id/edit(.:format)               work_experiences#edit
                   work_experience GET      /work_experiences/:id(.:format)                    work_experiences#show
                                   PATCH    /work_experiences/:id(.:format)                    work_experiences#update
                                   PUT      /work_experiences/:id(.:format)                    work_experiences#update
                                   DELETE   /work_experiences/:id(.:format)                    work_experiences#destroy
            newsletter_subscribers GET      /newsletter_subscribers(.:format)                  newsletter_subscribers#index
                                   POST     /newsletter_subscribers(.:format)                  newsletter_subscribers#create
         new_newsletter_subscriber GET      /newsletter_subscribers/new(.:format)              newsletter_subscribers#new
        edit_newsletter_subscriber GET      /newsletter_subscribers/:id/edit(.:format)         newsletter_subscribers#edit
             newsletter_subscriber GET      /newsletter_subscribers/:id(.:format)              newsletter_subscribers#show
                                   PATCH    /newsletter_subscribers/:id(.:format)              newsletter_subscribers#update
                                   PUT      /newsletter_subscribers/:id(.:format)              newsletter_subscribers#update
                                   DELETE   /newsletter_subscribers/:id(.:format)              newsletter_subscribers#destroy
              debug_params_preview GET      /previews/:id/debug_params(.:format)               previews#debug_params
              generate_css_preview GET      /previews/:id/generate_css(.:format)               previews#generate_css
                          previews GET      /previews(.:format)                                previews#index
                                   POST     /previews(.:format)                                previews#create
                       new_preview GET      /previews/new(.:format)                            previews#new
                      edit_preview GET      /previews/:id/edit(.:format)                       previews#edit
                           preview GET      /previews/:id(.:format)                            previews#show
                                   PATCH    /previews/:id(.:format)                            previews#update
                                   PUT      /previews/:id(.:format)                            previews#update
                                   DELETE   /previews/:id(.:format)                            previews#destroy
              fonts_resume_configs GET      /resume_configs/fonts(.:format)                    resume_configs#fonts
                    resume_configs GET      /resume_configs(.:format)                          resume_configs#index
                                   POST     /resume_configs(.:format)                          resume_configs#create
                 new_resume_config GET      /resume_configs/new(.:format)                      resume_configs#new
                edit_resume_config GET      /resume_configs/:id/edit(.:format)                 resume_configs#edit
                     resume_config GET      /resume_configs/:id(.:format)                      resume_configs#show
                                   PATCH    /resume_configs/:id(.:format)                      resume_configs#update
                                   PUT      /resume_configs/:id(.:format)                      resume_configs#update
                                   DELETE   /resume_configs/:id(.:format)                      resume_configs#destroy
                        dashboards GET      /dashboards(.:format)                              dashboards#index
                                   POST     /dashboards(.:format)                              dashboards#create
                     new_dashboard GET      /dashboards/new(.:format)                          dashboards#new
                    edit_dashboard GET      /dashboards/:id/edit(.:format)                     dashboards#edit
                         dashboard GET      /dashboards/:id(.:format)                          dashboards#show
                                   PATCH    /dashboards/:id(.:format)                          dashboards#update
                                   PUT      /dashboards/:id(.:format)                          dashboards#update
                                   DELETE   /dashboards/:id(.:format)                          dashboards#destroy
                resume_page_setups GET      /resume_page_setups(.:format)                      resume_page_setups#index
                                   POST     /resume_page_setups(.:format)                      resume_page_setups#create
             new_resume_page_setup GET      /resume_page_setups/new(.:format)                  resume_page_setups#new
            edit_resume_page_setup GET      /resume_page_setups/:id/edit(.:format)             resume_page_setups#edit
                 resume_page_setup GET      /resume_page_setups/:id(.:format)                  resume_page_setups#show
                                   PATCH    /resume_page_setups/:id(.:format)                  resume_page_setups#update
                                   PUT      /resume_page_setups/:id(.:format)                  resume_page_setups#update
                                   DELETE   /resume_page_setups/:id(.:format)                  resume_page_setups#destroy
                     cover_letters GET      /cover_letters(.:format)                           cover_letters#index
                                   POST     /cover_letters(.:format)                           cover_letters#create
                  new_cover_letter GET      /cover_letters/new(.:format)                       cover_letters#new
                 edit_cover_letter GET      /cover_letters/:id/edit(.:format)                  cover_letters#edit
                      cover_letter GET      /cover_letters/:id(.:format)                       cover_letters#show
                                   PATCH    /cover_letters/:id(.:format)                       cover_letters#update
                                   PUT      /cover_letters/:id(.:format)                       cover_letters#update
                                   DELETE   /cover_letters/:id(.:format)                       cover_letters#destroy
                     video_pitches GET      /video_pitches(.:format)                           video_pitches#index
                                   POST     /video_pitches(.:format)                           video_pitches#create
                   new_video_pitch GET      /video_pitches/new(.:format)                       video_pitches#new
                  edit_video_pitch GET      /video_pitches/:id/edit(.:format)                  video_pitches#edit
                       video_pitch GET      /video_pitches/:id(.:format)                       video_pitches#show
                                   PATCH    /video_pitches/:id(.:format)                       video_pitches#update
                                   PUT      /video_pitches/:id(.:format)                       video_pitches#update
                                   DELETE   /video_pitches/:id(.:format)                       video_pitches#destroy
                              root GET      /                                                  frontend/home#coming_soon
                          homepage GET      /homepage(.:format)                                frontend/home#index
                             login GET      /login(.:format)                                   frontend/home#auth
                            signup GET      /signup(.:format)                                  frontend/home#signup
                  auth_set_session POST     /auth/set_session(.:format)                        api#set_session
