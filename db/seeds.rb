user = User.create(email: 'qa@admin.com', password: '123456')
resume = Resume.create(user: user, name: 'Resume 1')
basic_info = BasicInfo.create(resume: resume, fname: 'Jonathan', mname: 'Senence', lname: 'Canaveral', 
			address: 'Phase 4 Blk14 Lot37 Di Makita St. Kadiliman City, Philippines 2200', 
			birthday: '1987/04/15', age: 30, bio_note: Faker::Lorem.words(10).join(' '))
