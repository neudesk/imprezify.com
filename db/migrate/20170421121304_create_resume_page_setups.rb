class CreateResumePageSetups < ActiveRecord::Migration
  def change
    create_table :resume_page_setups do |t|
    	t.string :page_size_type
      t.string :orientation
    	t.integer :width
    	t.integer :height
    	t.integer :margin_top
    	t.integer :margin_right
    	t.integer :margin_bottom
    	t.integer :margin_left
    	t.integer :resume_id
      t.timestamps null: false
    end
  end
end
