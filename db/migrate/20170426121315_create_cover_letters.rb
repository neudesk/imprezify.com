class CreateCoverLetters < ActiveRecord::Migration
  def up
    create_table :cover_letters do |t|
      t.integer :user_id
    	t.integer :resume_id
      t.string  :name
    	t.string	:address_to_date
    	t.string	:address_to_name
    	t.string	:address_to_gender
    	t.string	:address_to_title
    	t.string	:address_to_organization
    	t.string	:address_to_address
    	t.text		:content

      t.timestamps null: false
    end
  end

  def down
    drop_table :cover_letters
  end
end
