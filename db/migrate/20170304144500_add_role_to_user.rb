class AddRoleToUser < ActiveRecord::Migration
  def up
    # 1 customer
    # 2 support
    # 3 admin
    add_column :users, :role, :string, default: 1
  end

  def down
    remove_column :users, :role
  end
end
