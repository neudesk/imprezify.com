class CreateContactInfos < ActiveRecord::Migration
  def up
    create_table :contact_infos do |t|
      t.string :name
      t.string :value
      t.integer :resume_id
      t.timestamps null: false
    end
    remove_column :basic_infos, :phone
    remove_column :basic_infos, :mobile
    remove_column :basic_infos, :email
    remove_column :basic_infos, :website
  end

  def down
    drop_table :contact_infos
  end
end
