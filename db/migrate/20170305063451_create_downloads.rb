class CreateDownloads < ActiveRecord::Migration
  def up
    create_table :downloads do |t|
      t.string :downloaded_by
      t.integer :resume_id
      t.timestamps null: false
    end
  end

  def down
    drop_table :downloads
  end
end
