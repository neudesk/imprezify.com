class AddDegreeInEducation < ActiveRecord::Migration
  def up
  	add_column :educations, :degree, :string
  	add_column :educations, :degree_level, :string
  end

end
