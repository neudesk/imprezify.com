class CreateCharacterReferences < ActiveRecord::Migration
  def up
    create_table :character_references do |t|
	  t.string :fname
	  t.string :lname
	  t.string :mname
	  t.string :title
	  t.string :address
	  t.string :phone
	  t.string :mobile
	  t.string :email
	  t.integer :resume_id
      t.timestamps null: false
    end
  end

  def down
  	drop_table :character_references
  end
end
