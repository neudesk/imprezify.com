class CreateResumeConfigs < ActiveRecord::Migration
  def change
    create_table :resume_configs do |t|
	    t.string :target
      t.string :custom_css
      t.integer :margin_top
      t.integer :margin_bottom
      t.integer :margin_right
      t.integer :margin_left
      t.integer :padding_top
      t.integer :padding_right
      t.integer :padding_bottom
      t.integer :padding_left
      t.string :font_family
      t.string :font_style
      t.string :font_color
      t.integer :font_size
      t.string :text_override
      t.integer :border
      t.string :border_style
      t.string :border_color
      t.string :text_align
      t.integer :resume_id
      t.timestamps null: false
    end
    add_attachment :resume_configs, :background_image
  end

  def self.down
  	drop_table :resume_configs
  end
end