class AddTemplateIdToResume < ActiveRecord::Migration
  def change
    add_column :resumes, :template_id, :integer
  end
end
