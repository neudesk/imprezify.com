class CreateEducations < ActiveRecord::Migration
  def up
    create_table :educations do |t|
      t.string :course
      t.string :institute
      t.datetime :start_date
      t.datetime :end_date
      t.text :other_info
	  t.integer :resume_id
      t.timestamps null: false
    end
  end

  def down
  	drop_table :educations
  end
end
