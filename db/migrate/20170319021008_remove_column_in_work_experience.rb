class RemoveColumnInWorkExperience < ActiveRecord::Migration
  def up
  	remove_column :work_experiences, :is_present?
  	add_column :work_experiences, :current, :boolean, default: false
  end
end
