class CreateViews < ActiveRecord::Migration
  def up
    add_column :resumes, :status, :integer, default: 1 # 1: private, 2: public
    add_column :resumes, :is_paid?, :boolean, default: false
    create_table :views do |t|
      t.string :viewed_by
      t.integer :resume_id
      t.timestamps null: false
    end
  end

  def down
    remove_column :resumes, :status
    remove_column :resumes, :is_paid?
    drop_table :views
  end
end
