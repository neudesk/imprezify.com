class CreateVideoPitches < ActiveRecord::Migration
  def up
    create_table :video_pitches do |t|
      t.string :video_meta
      t.integer :user_id
      t.boolean :is_primary, default: false
      t.timestamps null: false
    end
    add_attachment :video_pitches, :video
  end

  def down
    drop_table :video_pitches
    remove_attachment :video_pitches, :video
  end
end
