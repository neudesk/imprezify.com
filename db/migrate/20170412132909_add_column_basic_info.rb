class AddColumnBasicInfo < ActiveRecord::Migration
  def up
  	add_column :basic_infos, :bio_note, :text
  end
  def down
  	remove_column :basic_infos, :bio_note
  end
end
