class CreateAttachments < ActiveRecord::Migration
  def up
    create_table :attachments do |t|
      t.timestamps null: false
      t.integer :attachable_id
      t.string :attachable_type
    end
    add_attachment :attachments, :data
  end

  def down
  	drop_table :attachments
  end	
end
