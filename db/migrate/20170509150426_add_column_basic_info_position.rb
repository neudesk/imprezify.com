class AddColumnBasicInfoPosition < ActiveRecord::Migration[5.0]
  def up
  	add_column :basic_infos, :position, :string
  end

  def down
  	remove_column :basic_infos, :position
  end
end
