class CreateResumes < ActiveRecord::Migration
  def up
    create_table :resumes do |t|
      t.string :name
      t.integer :user_id
      t.boolean :is_deleted
      t.timestamps null: false
    end
  end

  def down
  	drop_table :resumes
  end
end
