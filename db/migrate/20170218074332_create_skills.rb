class CreateSkills < ActiveRecord::Migration
  def up
    create_table :skills do |t|
      t.integer :resume_id
      t.string :name
      t.string :description
      t.integer :rate, default: 1
      t.timestamps null: false
    end
  end

  def down
    drop_table :skills
  end
end
