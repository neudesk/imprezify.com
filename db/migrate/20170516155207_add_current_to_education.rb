class AddCurrentToEducation < ActiveRecord::Migration[5.0]
  def up
    add_column :educations, :current, :boolean, default: false
  end

  def down
    remove_column :educations, :current
  end
end
