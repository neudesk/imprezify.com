class CreatePhotos < ActiveRecord::Migration
  def up
    create_table :photos do |t|
      t.integer :user_id
      t.integer :resume_id
      t.timestamps null: false
    end
    add_attachment :photos, :image
  end

  def down
  	drop_table :photos
  	remove_attachment :photos, :image
  end
end
