class AddDurationToVideoPitch < ActiveRecord::Migration[5.0]
  def up
    add_column :video_pitches, :duration, :string
  end

  def down
    remove_column :video_pitches, :duration
  end
end
