class CreateWorkExperiences < ActiveRecord::Migration
  def up
    create_table :work_experiences do |t|
  	  t.string :work_title
  	  t.string :company_name
  	  t.datetime :start_date
  	  t.datetime :end_date
  	  t.boolean :is_present?
  	  t.text :job_description
  	  t.integer :resume_id
      t.timestamps null: false
    end
  end

  def down
  	drop_table :work_experiences
  end
end
