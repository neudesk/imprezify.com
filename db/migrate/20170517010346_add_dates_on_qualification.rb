class AddDatesOnQualification < ActiveRecord::Migration[5.0]
  def up
    add_column :qualifications, :start_date, :datetime
    add_column :qualifications, :end_date, :datetime
  end

  def down
    remove_column :qualifications, :start_date
    remove_column :qualifications, :end_date
  end
end
