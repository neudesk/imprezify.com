class ChangeRateToDecimal < ActiveRecord::Migration
  def up
  	change_column :skills, :rate, :decimal, precision: 3, scale: 1
  end

  def down
    change_column :skills, :rate, :integer
  end
end
