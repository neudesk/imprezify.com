class CreateBasicInfos < ActiveRecord::Migration
  def up
    create_table :basic_infos do |t|
	  t.string :fname
	  t.string :lname
	  t.string :mname
	  t.text :address
	  t.string :city
	  t.string :street
	  t.string :state
	  t.string :province
	  t.string :zip
	  t.string :lat
	  t.string :lng
	  t.string :phone
	  t.string :mobile
	  t.string :email
	  t.string :website
	  t.integer	:age
	  t.datetime	:birthday
	  t.integer :resume_id
      t.timestamps null: false
    end
  end

  def down
  	drop_table :basic_infos
  end
end
