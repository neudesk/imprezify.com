class CreateQualifications < ActiveRecord::Migration
  def up
    create_table :qualifications do |t|
      t.string :name
      t.text :description
      t.integer :resume_id
      t.timestamps null: false
    end
  end

  def down
  	drop_table :qualifications
  end
end
