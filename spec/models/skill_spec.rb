require 'rails_helper'

RSpec.describe Skill, type: :model do

  context 'Validate' do

    let(:model) { build(:skill, rate: 6) }
    let(:field) { :rate }
    let(:error) { 'is not included in the list' }
    include_examples 'has_validation_error'

  end

end
