require 'rails_helper'

RSpec.describe Photo, type: :model do
  
	context 'photo uploads' do

		context 'validates associations' do
			let(:model) { Photo.reflect_on_association(:resume) }
			it 'should validate association with resume' do
				expect(model.macro).to eq(:belongs_to)
			end
		end

		context 'has valid user' do
			let(:model) { build(:photo) }
			let(:field) { :user }
			
			let(:error) { "can't be blank" }
			include_examples 'has_validation_error'
		end

		['must be less than 5 MB', 'is invalid'].each do |error|
			context 'has valid image file' do
				let(:model) { build(:photo, image: File.new(Rails.root.join('public', 'fixtures', 'sound.mp3')) ) }
				let(:field) { :image }
				let(:error) { error }
				include_examples 'has_validation_error'
			end
		end

		context 'able to create photo' do
			let(:model) { create(:photo, user: create(:user) ) }
			it 'should able to get image url' do
				expect(model.image.url.present?).to eq(true)
			end
		end

	end

end
