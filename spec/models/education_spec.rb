require 'rails_helper'

RSpec.describe Education, type: :model do
  context 'education information' do
  	
  	context 'validates associations' do
  		let(:model) { Education.reflect_on_association(:resume) }
  		it 'should validate association with resume' do
  			expect(model.macro).to eq(:belongs_to)
		end
	end

	%w(course institute start_date end_date other_info resume).each do |field|
		context "validate presence of #{field}" do
			let(:model) { build(:education, field.to_sym => nil) }
			let(:field) { field.to_sym }
			let(:error) { "can't be blank" }
			include_examples 'has_validation_error'
		end
	end

	context 'valid start date range' do
		let(:model) { build(:education, start_date: Time.now, end_date: Time.now - 1.hour) }
		let(:field) { :start_date }
		let(:error) { 'start date should not be greater to end date' }
		include_examples 'has_validation_error'
	end

	context 'valid end date range' do
		let(:model) { build(:education, start_date: Time.now + 1.minute, end_date: Time.now + 7.days) }
		let(:field) { :end_date }
		let(:error) { 'end date should not be a future date' }
		include_examples 'has_validation_error'
	end

  end
end
