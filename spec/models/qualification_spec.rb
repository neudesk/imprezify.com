require 'rails_helper'

RSpec.describe Qualification, type: :model do

  context 'validate polymorphic association to attachment' do
    let(:qualification) { create(:qualification, description: Faker::Lorem.words(3)) }
    let(:file) { File.new(Rails.root.join('public', 'fixtures', 'sound.mp3')) }

    it 'should have attachments' do
      qualification.attachments.create(data: file)
      expect(qualification.attachments.size).to eq(1)
    end

    it 'should have attachment url' do
      qualification.attachments.create(data: file)
      expect(/sound\.mp3/).to match(qualification.attachments.last.data.url)
    end

    context 'Multi attachments' do
      let(:files) { [file, file] }

      it 'should have multiple attachments' do
        qualification.upload_attachments(files)
        expect(qualification.attachments.size).to eq(files.size)
      end
    end

  end

end
