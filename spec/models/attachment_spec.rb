require 'rails_helper'

RSpec.describe Attachment, type: :model do
  
	context 'attachment file upload' do

		context 'validates associations' do
			let(:model) { Attachment.reflect_on_association(:attachable) }
			it 'should validate association with resume' do
				expect(model.macro).to eq(:belongs_to)
			end
    end

    context 'allow all file type' do
      let(:model) { create(:attachment) }
      let(:field) { :data }
      it 'should create attachment' do
				expect(model.data.url.present?).to eq(true)
      end
    end

		context 'has valid file size' do
			let(:model) { build(:attachment, data: File.new(Rails.root.join('public', 'fixtures', 'virtualbox.exe')) ) }
			let(:field) { :data }
			let(:error) { 'must be less than 10 MB' }
			include_examples 'has_validation_error'
		end

	end

end
