require 'rails_helper'

RSpec.describe CharacterReference, type: :model do

  context 'validates associations' do
    let(:model) { CharacterReference.reflect_on_association(:resume) }
    it 'should validate association with resume' do
      expect(model.macro).to eq(:belongs_to)
    end
  end

  %w(fname lname title address email resume).each do |field|
    context "validate presence of #{field}" do
      let(:model) { build(:character_reference, field.to_sym => nil) }
      let(:field) { field.to_sym }
      let(:error) { "can't be blank" }
      include_examples 'has_validation_error'
    end
  end

  context 'validates contact number' do
    let(:model) { build(:character_reference, phone: nil, mobile: nil) }
    let(:field) { :base }
    let(:error) { 'Contact number is required, please fill in mobile or phone number.' }
    include_examples 'has_validation_error'
  end

end
