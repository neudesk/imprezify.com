FactoryGirl.define do
  factory :qualification do
    name Faker::Superhero.name
    description Faker::Lorem.words(10)
  end
end
