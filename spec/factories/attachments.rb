FactoryGirl.define do
  factory :attachment do
    data { File.new(Rails.root.join('public', 'fixtures', 'sound.mp3')) }
  end
end
