FactoryGirl.define do
  factory :photo do
    image { File.new(Rails.root.join('public', 'fixtures', 'images', 'sample1.png')) }
  end
end
