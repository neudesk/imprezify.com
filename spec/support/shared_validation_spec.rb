require 'rails_helper'

RSpec.shared_examples 'has_validation_error' do
	it 'should validate error message' do
		model.valid?
		expect(model.errors[field]).to include(error)
	end
end