require 'rails_helper'

RSpec.describe SkillsController, type: 'request' do

  let(:user) { create(:user, password: 'duterte', confirmed_at: Time.now) }
  before(:each) do
    xhr :post, user_session_path, {email: user.email, password: 'duterte'}
    {
        'access-token': response.headers['access-token'],
        'token-type':   'Bearer',
        'client':       response.headers['client'],
        'expiry':       response.headers['expiry'],
        'uid':          response.headers['uid']
    }
    user.resumes.create(name: Faker::App.name)
  end
  let(:skill_params) do
    {
        name: Faker::Superhero.name,
        description: Faker::Lorem.words(10),
        rate: (1..5).to_a.sample
    }
  end
  let(:skill) do
    skill_params['resume_id'] = user.resumes.first.id
    create(:skill, skill_params)
  end

  it 'POST :create should able to create character reference' do
    skill_params['resume_id'] = user.resumes.first.id
    xhr :post, skills_path(skill: skill_params), headers
    expect(response).to have_http_status(:success)
    expect(JSON.parse(response.body)['name']).to eq(skill_params[:name])
  end

  it 'GET :index should list all skills' do
    skill
    xhr :get, skills_path(resume_id: user.resumes.first.id), headers
    expect(response).to have_http_status(:success)
    expect(JSON.parse(response.body).size).to eq(1)
  end

  it 'GET :show should show a skills' do
    xhr :get, skill_path(skill), headers
    expect(response).to have_http_status(:success)
    expect(JSON.parse(response.body)['description']).to eq(skill.description)
  end

  it 'PUT :update should update a skills' do
    xhr :put, skill_path(id: skill.id, skill: { description: 'Word and words' }), headers
    expect(response).to have_http_status(:success)
    expect(JSON.parse(response.body)['description']).to eq('Word and words')
  end

  it 'DELETE :destroy should able to delete skills' do
    xhr :delete, skill_path(skill), headers
    expect(response).to have_http_status(:success)
  end

end
