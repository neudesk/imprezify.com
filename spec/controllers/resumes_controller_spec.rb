require 'rails_helper'

RSpec.describe ResumesController, type: 'request' do

  context 'api' do
    let(:user) { create(:user, password: 'duterte', confirmed_at: Time.now) }
    before(:each) do
      xhr :post, user_session_path, {email: user.email, password: 'duterte'}
      {
          'access-token': response.headers['access-token'],
          'token-type':   'Bearer',
          'client':       response.headers['client'],
          'expiry':       response.headers['expiry'],
          'uid':          response.headers['uid']
      }
      user.resumes.create(name: Faker::App.name)
    end

    it 'POST :create should able to create resume' do
      xhr :post, resumes_path(resume: { name: 'Assassin' }), headers
      expect(response).to have_http_status(:success)
      expect(JSON.parse(response.body)['name']).to eq('Assassin')
    end

    it 'GET :index should list all current users resumes' do
      xhr :get, resumes_path, headers
      expect(response).to have_http_status(:success)
      expect(JSON.parse(response.body).first['name']).to eq(user.resumes.first.name)
    end

    it 'GET :show should show resume' do
      xhr :get, resume_path(user.resumes.last), headers
      expect(response).to have_http_status(:success)
      expect(JSON.parse(response.body)['name']).to eq(user.resumes.first.name)
    end

    it 'DELETE :destroy should destroy resume' do
      xhr :delete, resume_path(user.resumes.last), headers
      expect(response).to have_http_status(:success)
      expect(JSON.parse(response.body)['deleted']).to eq(true)
    end

    it 'PUT :update should update resume' do
      xhr :put, resume_path(id: user.resumes.last.id, resume: {name: 'Changed'} ), headers
      expect(response).to have_http_status(:success)
      expect(JSON.parse(response.body)['name']).to eq('Changed')
    end

  end

end
