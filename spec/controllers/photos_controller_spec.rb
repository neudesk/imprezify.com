require 'rails_helper'

RSpec.describe PhotosController, type: 'request' do

  let(:user) { create(:user, password: 'duterte', confirmed_at: Time.now) }
  before(:each) do
    xhr :post, user_session_path, {email: user.email, password: 'duterte'}
    {
        'access-token': response.headers['access-token'],
        'token-type':   'Bearer',
        'client':       response.headers['client'],
        'expiry':       response.headers['expiry'],
        'uid':          response.headers['uid']
    }
    user.resumes.create(name: Faker::App.name)
  end
  let(:photo_params) do
    {
        image: Faker::LoremPixel.image
    }
  end
  let(:photo) do
    photo_params['resume_id'] = user.resumes.first.id
    photo_params['user_id'] = user.id
    create(:photo, photo_params)
  end

  it 'POST :create should able to create character reference' do
    photo_params['resume_id'] = user.resumes.first.id
    photo_params['user_id'] = user.id
    xhr :post, photos_path(photo: photo_params), headers
    expect(response).to have_http_status(:success)
    expect(JSON.parse(response.body).present?).to eq(true)
  end

  it 'GET :index should list all photos' do
    photo
    xhr :get, photos_path, headers
    expect(response).to have_http_status(:success)
    expect(JSON.parse(response.body).size).to eq(1)
  end

  it 'GET :show should show a photos' do
    xhr :get, photo_path(photo), headers
    expect(response).to have_http_status(:success)
    expect(JSON.parse(response.body)['id']).to eq(photo.id)
  end

  it 'GET :show should have url nodes' do
    xhr :get, photo_path(photo), headers
    expect(response).to have_http_status(:success)
    expect(JSON.parse(response.body)['medium'].present?).to eq(true)
    expect(JSON.parse(response.body)['thumb'].present?).to eq(true)
    expect(JSON.parse(response.body)['original'].present?).to eq(true)
  end

  it 'PUT :update should update a photos' do
    xhr :put, photo_path(id: photo.id, photo: { image: Faker::LoremPixel.image }), headers
    expect(response).to have_http_status(:success)
  end

  it 'DELETE :destroy should able to delete photos' do
    xhr :delete, photo_path(photo), headers
    expect(response).to have_http_status(:success)
  end

end
