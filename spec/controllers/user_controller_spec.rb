require 'rails_helper'

RSpec.describe 'Authentication', type: 'request' do

    context 'Normal auth' do
      let(:user) { create(:user, password: 'duterte', confirmed_at: Time.now) }
      it 'should able to login via api do' do
        xhr :post, user_session_path, {email: user.email, password: 'duterte'}
        expect(response).to have_http_status(:success)
      end

      it 'should able to signup' do
        xhr :post, user_registration_path, { email: Faker::Internet.email, password: 'ABC12abc', password_confirmation: 'ABC12abc'  }
        expect(response).to have_http_status(:success)
      end

    end

end


