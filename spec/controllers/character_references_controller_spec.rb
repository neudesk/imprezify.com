require 'rails_helper'

RSpec.describe CharacterReferencesController, type: 'request' do

  context 'api' do

    let(:user) { create(:user, password: 'duterte', confirmed_at: Time.now) }
    before(:each) do
      xhr :post, user_session_path, {email: user.email, password: 'duterte'}
      {
          'access-token': response.headers['access-token'],
          'token-type':   'Bearer',
          'client':       response.headers['client'],
          'expiry':       response.headers['expiry'],
          'uid':          response.headers['uid']
      }
      user.resumes.create(name: Faker::App.name)
    end
    let(:character_reference_params) do
      {
        fname: Faker::Name.first_name, lname: Faker::Name.last_name, mname: Faker::Name.last_name,
        title: Faker::Superhero.name, address: Faker::Address.street_name,
        mobile: Faker::PhoneNumber.cell_phone, email: Faker::Internet.email
      }
    end
    let(:character_reference) do
      character_reference_params['resume_id'] = user.resumes.first.id
      create(:character_reference, character_reference_params)
    end

    it 'POST :create should able to create character reference' do
      character_reference_params['resume_id'] = user.resumes.first.id
      xhr :post, character_references_path(character_reference: character_reference_params), headers
      expect(response).to have_http_status(:success)
      expect(JSON.parse(response.body)['fname']).to eq(character_reference_params[:fname])
    end

    it 'GET :index should list all character references' do
      character_reference
      xhr :get, character_references_path(resume_id: user.resumes.first.id), headers
      expect(response).to have_http_status(:success)
      expect(JSON.parse(response.body).size).to eq(1)
    end

    it 'GET :show should show a character references' do
      xhr :get, character_reference_path(character_reference), headers
      expect(response).to have_http_status(:success)
      expect(JSON.parse(response.body)['fname']).to eq(character_reference.fname)
    end

    it 'PUT :update should update a character references' do
      xhr :put, character_reference_path(id: character_reference.id, character_reference: { fname: 'Duterte' }), headers
      expect(response).to have_http_status(:success)
      expect(JSON.parse(response.body)['fname']).to eq('Duterte')
    end

    it 'DELETE :destroy should able to delete character references' do
      xhr :delete, character_reference_path(character_reference), headers
      expect(response).to have_http_status(:success)
    end

  end

end
