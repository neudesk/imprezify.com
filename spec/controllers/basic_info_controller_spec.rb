require 'rails_helper'

RSpec.describe BasicInfoController, type: 'request' do

  context 'api' do
    let(:user) { create(:user, password: 'duterte', confirmed_at: Time.now) }
    let(:basic_info_params) do
      { fname: Faker::Name.first_name, lname: Faker::Name.last_name, age: 20,
        birthday: Faker::Date.between(29.years.ago, Date.today), mobile: Faker::PhoneNumber.cell_phone,
        address: Faker::Address.street_address, email: Faker::Internet.email
      }
    end
    let(:basic_info) do
      user.resumes.create(name: Faker::App.name)
      basic_info_params['resume_id'] = user.resumes.first.id
      create(:basic_info, basic_info_params)
    end

    before(:each) do
      xhr :post, user_session_path, {email: user.email, password: 'duterte'}
      {
          'access-token': response.headers['access-token'],
          'token-type':   'Bearer',
          'client':       response.headers['client'],
          'expiry':       response.headers['expiry'],
          'uid':          response.headers['uid']
      }
      user.resumes.create(name: Faker::App.name)
    end

    it 'POST :create should able to create basic_info' do
      basic_info_params['resume_id'] = user.resumes.first.id
      xhr :post, basic_info_index_path(basic_info: basic_info_params), headers
      expect(response).to have_http_status(:success)
      expect(JSON.parse(response.body).present?).to eq(true)
      expect(JSON.parse(response.body)['resume_id']).to eq(user.resumes.first.id)
    end

    it 'GET :show should able to show basic_info' do
      xhr :get, basic_info_path(basic_info), headers
      expect(response).to have_http_status(:success)
      expect(JSON.parse(response.body).present?).to eq(true)
      expect(JSON.parse(response.body)['fname']).to eq(basic_info.fname)
    end

    it 'PUT :update should able to update show basic_info' do
      xhr :put, basic_info_path(id: basic_info.id, basic_info: { fname: 'Jonathan' }), headers
      expect(response).to have_http_status(:success)
      expect(JSON.parse(response.body).present?).to eq(true)
      expect(JSON.parse(response.body)['fname']).to eq('Jonathan')
    end

    it 'DELETE :destroy should able to destroy show basic_info' do
      xhr :delete, basic_info_path(basic_info), headers
      expect(response).to have_http_status(:success)
      expect(JSON.parse(response.body).present?).to eq(true)
    end

  end

end
