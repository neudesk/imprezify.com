require 'rails_helper'
include ActionDispatch::TestProcess

RSpec.describe WorkExperiencesController, type: 'request' do

  let(:user) { create(:user, password: 'duterte', confirmed_at: Time.now) }
  before(:each) do
    xhr :post, user_session_path, {email: user.email, password: 'duterte'}
    {
        'access-token': response.headers['access-token'],
        'token-type':   'Bearer',
        'client':       response.headers['client'],
        'expiry':       response.headers['expiry'],
        'uid':          response.headers['uid']
    }
    user.resumes.create(name: Faker::App.name)
  end
  let(:work_experience_params) do
    {
        work_title: Faker::Superhero.name,
        company_name: Faker::Company.name,
        start_date: Time.now - 7.years,
        end_date: Time.now - 1.month,
        is_present?: false,
        job_description: Faker::Lorem.words(10)
    }
  end
  let(:work_experience) do
    work_experience_params['resume_id'] = user.resumes.first.id
    create(:work_experience, work_experience_params)
  end
  let(:files) { [File.new( Rails.root.join('public', 'fixtures', 'sound.mp3') ), File.new( Rails.root.join('public', 'fixtures', 'doc.docx') )] }

  it 'POST :create should able to create character reference' do
    work_experience_params['resume_id'] = user.resumes.first.id
    xhr :post, work_experiences_path(work_experience: work_experience_params), headers
    expect(response).to have_http_status(:success)
    expect(JSON.parse(response.body)['work_title']).to eq(work_experience_params[:work_title])
  end

  it 'GET :index should list all work_experiences' do
    work_experience
    xhr :get, work_experiences_path(resume_id: user.resumes.first.id), headers
    expect(response).to have_http_status(:success)
    expect(JSON.parse(response.body).size).to eq(1)
  end

  it 'GET :show should show a work_experiences' do
    xhr :get, work_experience_path(work_experience), headers
    expect(response).to have_http_status(:success)
    expect(JSON.parse(response.body)['job_description']).to eq(work_experience.job_description)
  end

  it 'PUT :update should update a work_experiences' do
    xhr :put, work_experience_path(id: work_experience.id, work_experience: { job_description: 'Word and words' }), headers
    expect(response).to have_http_status(:success)
    expect(JSON.parse(response.body)['job_description']).to eq('Word and words')
  end

  it 'DELETE :destroy should able to delete work_experiences' do
    xhr :delete, work_experience_path(work_experience), headers
    expect(response).to have_http_status(:success)
  end

  it 'GET :attachments should list all work_experiences attachments' do
    files.each do |file|
      work_experience.attachments.create!(data: file)
    end
    xhr :get, attachments_work_experience_path(id: work_experience.id), headers
    expect(response).to have_http_status(:success)
    expect(JSON.parse(response.body).size).to eq(work_experience.attachments.size)
  end

  it 'POST :upload_attachment should upload_attachment in work_experiences attachments' do
    xhr :post, upload_attachment_work_experience_path(id: work_experience.id, attachments: files.map { |file| Rack::Test::UploadedFile.new(file) }), headers
    expect(response).to have_http_status(:success)
    expect(JSON.parse(response.body).size).to eq(files.size)
    expect(JSON.parse(response.body).first['url'].present?).to eq(true)
  end

  it 'POST :destroy_attachment should destroy work_experiences attachment' do
    work_experience.attachments.create!(data: files.first)
    xhr :post, destroy_attachment_work_experience_path(id: work_experience.id, attachment_id: work_experience.attachments.first.id), headers
    expect(response).to have_http_status(:success)
  end

end
