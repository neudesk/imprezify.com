require 'rails_helper'

RSpec.describe EducationsController, type: 'request' do

  let(:user) { create(:user, password: 'duterte', confirmed_at: Time.now) }
  before(:each) do
    xhr :post, user_session_path, {email: user.email, password: 'duterte'}
    {
        'access-token': response.headers['access-token'],
        'token-type':   'Bearer',
        'client':       response.headers['client'],
        'expiry':       response.headers['expiry'],
        'uid':          response.headers['uid']
    }
    user.resumes.create(name: Faker::App.name)
  end
  let(:education_params) do
    {
        course: Faker::App.name,
        institute: Faker::Company.name,
        start_date: Time.now - 5.years,
        end_date: Time.now - 1.year,
        other_info: Faker::Lorem.words(10).join(' ')
    }
  end
  let(:education) do
    education_params['resume_id'] = user.resumes.first.id
    create(:education, education_params)
  end

  it 'POST :create should able to create education' do
    education_params['resume_id'] = user.resumes.first.id
    xhr :post, educations_path(education: education_params), headers
    expect(response).to have_http_status(:success)
    expect(JSON.parse(response.body)['fname']).to eq(education_params[:fname])
  end

  it 'GET :index should list all educations' do
    education
    xhr :get, educations_path(resume_id: user.resumes.first.id), headers
    expect(response).to have_http_status(:success)
    expect(JSON.parse(response.body).size).to eq(1)
  end

  it 'GET :show should show a educations' do
    xhr :get, education_path(education), headers
    expect(response).to have_http_status(:success)
    expect(JSON.parse(response.body)['institute']).to eq(education.institute)
  end

  it 'PUT :update should update a educations' do
    xhr :put, education_path(id: education.id, education: { course: 'Information Technology' }), headers
    expect(response).to have_http_status(:success)
    expect(JSON.parse(response.body)['course']).to eq('Information Technology')
  end

  it 'DELETE :destroy should able to delete educations' do
    xhr :delete, education_path(education), headers
    expect(response).to have_http_status(:success)
  end

end
