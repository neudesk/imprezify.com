require 'rails_helper'

RSpec.describe QualificationsController, type: 'request' do

  let(:user) { create(:user, password: 'duterte', confirmed_at: Time.now) }
  before(:each) do
    xhr :post, user_session_path, {email: user.email, password: 'duterte'}
    {
        'access-token': response.headers['access-token'],
        'token-type':   'Bearer',
        'client':       response.headers['client'],
        'expiry':       response.headers['expiry'],
        'uid':          response.headers['uid']
    }
    user.resumes.create(name: Faker::App.name)
  end
  let(:qualification_params) do
    {
        name: Faker::Superhero.power,
        description: Faker::Lorem.words(10).join(' ')
    }
  end
  let(:qualification) do
    qualification_params['resume_id'] = user.resumes.first.id
    create(:qualification, qualification_params)
  end

  it 'POST :create should able to create qualification' do
    qualification_params['resume_id'] = user.resumes.first.id
    xhr :post, qualifications_path(qualification: qualification_params), headers
    expect(response).to have_http_status(:success)
    expect(JSON.parse(response.body)['fname']).to eq(qualification_params[:fname])
  end

  it 'GET :index should list all qualifications' do
    qualification
    xhr :get, qualifications_path(resume_id: user.resumes.first.id), headers
    expect(response).to have_http_status(:success)
    expect(JSON.parse(response.body).size).to eq(1)
  end

  it 'GET :show should show a qualifications' do
    xhr :get, qualification_path(qualification), headers
    expect(response).to have_http_status(:success)
    expect(JSON.parse(response.body)['description']).to eq(qualification.description)
  end

  it 'PUT :update should update a qualifications' do
    xhr :put, qualification_path(id: qualification.id, qualification: { description: 'Word and words' }), headers
    expect(response).to have_http_status(:success)
    expect(JSON.parse(response.body)['description']).to eq('Word and words')
  end

  it 'DELETE :destroy should able to delete qualifications' do
    xhr :delete, qualification_path(qualification), headers
    expect(response).to have_http_status(:success)
  end

end
