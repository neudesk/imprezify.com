require 'rails_helper'

RSpec.describe NewsletterSubscribersController, type: 'request' do

	context 'request' do
		let(:email) { Faker::Internet.email }
		let(:subscriber) { create(:newsletter_subscriber, email: email) }
		
		it 'should raise invalid record' do
			xhr :post, newsletter_subscribers_path(email: subscriber.email )
			expect(response).to have_http_status(422)
		end
		
		it 'should able to subscribe' do
			xhr :post, newsletter_subscribers_path(email: Faker::Internet.email)
			expect(response).to have_http_status(:success)
			expect(NewsletterSubscriber.all.count).to eq(1)	
		end
	end

end
