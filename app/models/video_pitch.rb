class VideoPitch < ActiveRecord::Base
  has_attached_file :video, :styles => {
      :webm => { :geometry => "800x600", :format => 'webm' },
      :mp4 => { :geometry => "800x600", :format => 'mp4' },
      :thumb => { :geometry => "168x94#", :format => 'png', :time => 1 }
  }, :processors => [:transcoder]
  validates_attachment :video, content_type: { content_type: ['video/webm'] }

  validate :set_maximum_videos, on: [:create]
  before_save :clear_primary

  def url_nodes
    video_json = as_json
    video_json['webm'] = video.url(:webm)
    video_json['mp4'] = video.url(:mp4)
    video_json['thumb'] = video.url(:thumb)
    video_json
  end

  private

  def clear_primary
    self.class.where(user_id: self.user_id).update_all(is_primary: false) if self.is_primary == true
  end

  def set_maximum_videos
    count = self.class.where(user_id: self.user_id).count
    self.errors.add(:base, "You can only add maximum of #{MAX_VIDEO_COUNT}, please delete one of your videos.") unless count < MAX_VIDEO_COUNT
  end

end
