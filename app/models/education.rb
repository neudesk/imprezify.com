class Education < ActiveRecord::Base
	belongs_to :resume
	has_many :attachments, as: :attachable

	class << self
		def with_attachments(id)
			result = []
			educations = Education.where(resume_id: id).order('start_date DESC')
			educations.each do |education|
				data = education.as_json
				data['attachments'] = education.get_attachments
				result << data
			end
			result
		end
	end


	def get_attachments
		res = []
		attachments.each do |attachment|
			attachment_json = attachment.as_json
			attachment_json['url'] = attachment.data.url
			res << attachment_json
		end
		res
	end

	def upload_attachments(files)
		files.each do |key, file|
			attachments.create!(data: file)
		end
		reload
		get_attachments
	end

end
