class Attachment < ActiveRecord::Base
	has_attached_file :data
	belongs_to :attachable, polymorphic: true
	do_not_validate_attachment_file_type :data
	validates_attachment_size :data, :less_than => 10.megabytes
end
