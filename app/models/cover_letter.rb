class CoverLetter < ActiveRecord::Base
	belongs_to :resume
	belongs_to :user

	class << self
		def all_with_resume_details(user)
			 conver_letters = user.cover_letters
	 		 conver_letters.map { |x| x.with_resume_details }
		end
	end

	def with_resume_details
		json_cover_letter = self.as_json
		json_cover_letter['resume'] = resume
		json_cover_letter
	end

end
