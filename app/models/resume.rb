
 class Resume < ActiveRecord::Base
	belongs_to :user
	has_one :photo
	has_one :basic_info
	has_many :work_experiences
	has_many :qualifications
	has_many :educations
	has_many :character_references
	has_one :bio_note
	has_many :skills
	has_many :views
	has_many :downloads
	has_many :contact_info
	has_many :resume_configs
	has_one :resume_page_setup
	has_one :cover_letter

	default_scope { where(is_deleted: false) }

	validates :name, uniqueness: true, presence: true

	class << self
		def get_template
			File.read(Rails.root.join('public/resume_templates/0001/index.html'))
		end

		def resumes_with_stats(user)
			result = []
			resumes = user.resumes.includes(:views, :downloads)
			unless resumes.blank?
				resumes.each do |resume|
					data = resume.as_json
					data['view_count'] = resume.views.count
					data['view_count'] = resume.views.count
					data['last_viewed_by'] = resume.views.last.try(:viewed_by)
					data['download_count'] = resume.downloads.count
					result << data
				end
			end
			result
		end

		def create_cover_url(id, size)
			ActionController::Base.helpers.asset_path("template_assets/#{id}/cover/#{size}.png")
		end

		def template_details
			result = []
			Dir.glob(Rails.root.join('app/views/previews/resume_templates/**/*.yml')) do |file|
				result << YAML.load_file(file)
			end
			result.each do |x|
				x['cover'] = {
						large: create_cover_url(x['template']['id'], 'large'),
						medium: create_cover_url(x['template']['id'], 'medium')
				}
			end
			result
		end

	end

	def tree
		{
			id: id, 
			name: name, 
			template_id: template_id,
			created_at: created_at, 
			updated_at: updated_at,
			cover_letter: cover_letter.present? ? cover_letter : nil,
			photo: photo.present? ? photo.url_nodes : nil,
			basic_info: basic_info,
			contact_info: contact_info,
			work_experiences: WorkExperience.with_attachments(id),
			educations: Education.with_attachments(id),
			qualifications: Qualification.with_attachments(id),
			skills: skills,
			character_references: character_references
		}	
	end

end
