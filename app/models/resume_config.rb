require 'base64'


class ResumeConfig < ActiveRecord::Base
	belongs_to :resume

	FONTS = [
		"Impact, Charcoal, sans-serif",
		"‘Palatino Linotype’, ‘Book Antiqua’, Palatino, serif",
		"Tahoma, Geneva, sans-serif",
		"Century Gothic, sans-serif",
		"‘Lucida Sans Unicode’, ‘Lucida Grande’, sans-serif",
		"‘Arial Black’, Gadget, sans-serif",
		"‘Times New Roman’, Times, serif",
		"‘Arial Narrow’, sans-serif",
		"Verdana, Geneva, sans-serif",
		"Copperplate / Copperplate Gothic Light, sans-serif",
		"‘Lucida Console’, Monaco, monospace",
		"Gill Sans / Gill Sans MT, sans-serif",
		"‘Trebuchet MS’, Helvetica, sans-serif",
		"‘Courier New’, Courier, monospace",
		"Arial, Helvetica, sans-serif",
		"Georgia, Serif"
	]
end