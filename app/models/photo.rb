class Photo < ActiveRecord::Base
	belongs_to :user
	belongs_to :resume
	has_attached_file :image, styles: { medium: '302x378>', thumb: '151x189>' }, default_url: '/images/:style/missing.png'
	validates_attachment :image, content_type: { content_type: ['image/jpeg', 'image/gif', 'image/png'] }
	validates_attachment_size :image, :less_than => 5.megabytes
	validates :user, presence: true

	before_save :unset_primary
	
	def url_nodes
		photo_json = as_json
		photo_json['medium'] = image.url(:medium)
		photo_json['thumb'] = image.url(:thumb)
		photo_json['original'] = image.url(:original)
		photo_json['path'] = image.path
		photo_json
	end

	def unset_primary
		self.class.all.update_all(resume_id: nil) unless self.resume_id.blank?
	end

end
