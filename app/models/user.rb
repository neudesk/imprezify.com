class User < ActiveRecord::Base
  # Include default devise modules.
  devise :database_authenticatable, :registerable,
          :recoverable, :rememberable, :trackable,
         :validatable, :omniauthable
  include DeviseTokenAuth::Concerns::User

  has_many :resumes
  has_many :photos
  has_many :cover_letters
  has_many :video_pitches

  def admin?
    role == 3
  end

  def supporter
    role == 2
  end

  def customer
    role == 1
  end

  def get_video_piches
    res = []
    video_pitches.each do |video|
      res << video.url_nodes
    end
    res
  end

  def get_photos
    res = []
    photos.each do |photo|
      res << photo.url_nodes
    end
    res
  end

  def upload_photos(files)
    unless files.blank?
      files.each do |key, file|
        photos.create!(image: file)
      end
      reload
      get_photos
    end
  end

end
