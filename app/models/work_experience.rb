class WorkExperience < ActiveRecord::Base
	belongs_to :resume
	has_many :attachments, as: :attachable

	class << self
		def with_attachments(id)
			result = []
			experiences = WorkExperience.where(resume_id: id)
			experiences.each do |experience|
				data = experience.as_json
				data['attachments'] = experience.get_attachments
				result << data
			end
			result
		end
	end

	def get_attachments
		res = []
		attachments.each do |attachment|
			attachment_json = attachment.as_json
			attachment_json['url'] = attachment.data.url
			res << attachment_json
		end
		res
	end

	def upload_attachments(files)
		unless files.blank?
			files.each do |key, file|
				attachments.create!(data: file)
			end
			reload
			get_attachments
		end
	end

end
