class CharacterReference < ActiveRecord::Base
	belongs_to :resume
	# validates :fname, :lname, :title, :address, :resume, :email, presence: true
	# validate :contact_number

	private

	def contact_number
		errors.add(:base, 'Contact number is required, please fill in mobile or phone number.') unless phone.present? || mobile.present?
	end
end
