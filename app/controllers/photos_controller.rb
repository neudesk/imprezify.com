class PhotosController < ApiController

  before_action :authenticate_user!
  before_action :set_resource, only: [:index, :show, :update, :destroy]

  def index
    render json: current_user.photos.map { |photo| photo.url_nodes }
  end

  def create
    render json: current_user.upload_photos(params[:photo])
  end

  def show
    render json: @photo.url_nodes
  end

  def update
    @photo.update_attributes!(valid_params)
    render json: @photo.reload.url_nodes
  end

  def destroy
    render json: @photo.destroy
  end

  private

  def valid_params
    params.require(:photo).permit( Photo.column_names.map { |x| x.to_sym } )
  end

  def set_resource
    @resume = current_user.resumes.find(params[:resume_id]) unless params[:resume_id].blank?
    @photo = Photo.find(params[:id]) unless params[:id].blank?
  end

end
