class ResumesController < ApiController
  require 'erb'
  before_action :authenticate_user!
  before_action :set_resource, except: [:index, :create, :resume_template_details]

  def index
    render json: Resume.resumes_with_stats(current_user)
  end

  def create
    render json: current_user.resumes.create!(valid_params)
  end

  def show
    resume = current_user.resumes.includes(:basic_info, :contact_info).find(params[:id])
    result = resume.as_json
    result['basic_info_id'] = resume.basic_info.try(:id)
    result['contact_info_id'] = resume.contact_info.try(:id)
    result['photo_primary'] = resume.photo.try(:url_nodes)
    render json: result
  end

  def destroy
    @resume.update_attribute(:is_deleted, true)
    render json: { deleted: @resume.reload.is_deleted }
  end

  def update
    @resume.update_attributes(valid_params)
    render json: @resume.reload
  end

  def resume_template_details
    render json: Resume.template_details
  end

  private

  def valid_params
    params.require(:resume).permit(Resume.column_names.map {|x| x.to_sym })
  end

  def set_resource
    @resume = current_user.resumes.find(params[:id])
  end

end
