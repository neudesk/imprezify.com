class QualificationsController < ApiController

  before_action :authenticate_user!
  before_action :set_resource, except: [:create, :index]

  def index
    render json: Qualification.with_attachments(params[:resume_id])
  end

  def create
    render json: Qualification.create!(valid_params)
  end

  def show
    render json: @qualification
  end

  def update
    @qualification.update_attributes!(valid_params)
    render json: @qualification.reload
  end

  def destroy
    render json: @qualification.destroy
  end

  def attachments
    render json: @qualification.get_attachments
  end

  def upload_attachment
    render json: @qualification.upload_attachments(params[:file])
  end

  def destroy_attachment
    render json: @qualification.attachments.find(params[:attachment_id]).try(:destroy)
  end

  private

  def valid_params
    params.require(:qualification).permit( Qualification.column_names.map { |x| x.to_sym } )
  end

  def set_resource
    @resume = current_user.resumes.find(params[:resume_id]) unless params[:resume_id].blank?
    @qualification = Qualification.find(params[:id]) unless params[:id].blank?
  end

end
