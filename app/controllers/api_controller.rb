class ApiController < ActionController::API
  include DeviseTokenAuth::Concerns::SetUserByToken

  rescue_from ActiveRecord::RecordNotFound, with: :handle_record_not_found
  rescue_from ActiveRecord::RecordInvalid, with: :handle_invalid_record
  rescue_from ActiveRecord::AssociationNotFoundError, with: :handle_association_error
  rescue_from NoMethodError, with: :handle_no_method
  rescue_from EOFError, with: :handle_end_of_file

  def handle_record_not_found
    error_handler({ errors: ['Record can\'t be found'] }, 404)
  end

  def handle_invalid_record(e)
    error_handler({ message: e.message, errors: e.record.errors.full_messages }, 422)
  end

  def handle_association_error(e)
    error_handler({ errors: ['Unprocessable request'], stacktrace: e.inspect }, 422)
  end

  def handle_no_method(e)
    error_handler({ errors: ['Unprocessable request'], stacktrace: e.inspect }, 500)
  end

  def handle_end_of_file(e)
    error_handler({ errors: ['Invalid file'], stacktrace: e.inspect }, 500)
  end

  private

  def error_handler(data, status)
    render json: data, status: status
  end
end
