class ContactInfoController < ApplicationController
  before_action :authenticate_user!
  before_action :set_resource, only: [:show, :update, :destroy]

  def index
    resume = current_user.resumes.find(params[:resume_id])
    render json: resume.contact_info
  end

  def show
    render json: @contact_info
  end

  def create
    render json: ContactInfo.create!(valid_params)
  end

  def update
    @contact_info.update_attributes!(valid_params)
    render json: @contact_info
  end

  def destroy
    render json: @contact_info.destroy
  end

  private

  def valid_params
    params.require(:contact_info).permit(ContactInfo.column_names.map {|x| x.to_sym })
  end

  def set_resource
    @contact_info = ContactInfo.find(params[:id]) unless params[:id].blank?
  end
end
