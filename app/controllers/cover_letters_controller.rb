class CoverLettersController < ApplicationController

	# before_action :authenticate_user!
  before_action :set_resource, except: [:index, :create]

  def index
    render json: CoverLetter.all_with_resume_details(current_user)
  end

  def show
    render json: @resource
  end

  def create
    render json: current_user.cover_letters.create!(valid_params)
  end

  def update
    @resource.update_attributes!(valid_params)
    render json: @resource.reload
  end

  def destroy
    render json: @resource.destroy
  end

  private

  def valid_params
    params.require(:cover_letter).permit( CoverLetter.column_names.map { |x| x.to_sym } )
  end

  def set_resource
    @resource = CoverLetter.find(params[:id]) unless params[:id].blank?
  end

end
