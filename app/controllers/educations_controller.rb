class EducationsController < ApiController

  before_action :authenticate_user!
  before_action :set_resource, except: [:create, :index]

  def index
    render json: Education.with_attachments(params[:resume_id])
  end

  def create
    render json: Education.create!(valid_params)
  end

  def show
    render json: @education
  end

  def update
    @education.update_attributes!(valid_params)
    render json: @education.reload
  end

  def destroy
    render json: @education.destroy
  end

  def attachments
    render json: @education.get_attachments
  end

  def upload_attachment
    render json: @education.upload_attachments(params[:file])
  end

  def destroy_attachment
    render json: @education.attachments.find(params[:attachment_id]).try(:destroy)
  end

  private

  def valid_params
    params.require(:education).permit( Education.column_names.map { |x| x.to_sym } )
  end

  def set_resource
    @resume = current_user.resumes.find(params[:resume_id]) unless params[:resume_id].blank?
    @education = Education.find(params[:id]) unless params[:id].blank?
  end

end
