class SkillsController < ApiController

  before_action :authenticate_user!
  before_action :set_resource, only: [:index, :show, :update, :destroy]

  def index
    render json: @resume.skills
  end

  def create
    render json: Skill.create!(valid_params)
  end

  def show
    render json: @skill
  end

  def update
    @skill.update_attributes!(valid_params)
    render json: @skill.reload
  end

  def destroy
    render json: @skill.destroy
  end

  private

  def valid_params
    params.require(:skill).permit( Skill.column_names.map { |x| x.to_sym } )
  end

  def set_resource
    @resume = current_user.resumes.find(params[:resume_id]) unless params[:resume_id].blank?
    @skill = Skill.find(params[:id]) unless params[:id].blank?
  end

end
