class BasicInfoController < ApiController

  before_action :authenticate_user!
  before_action :set_resource, only: [:show, :update, :destroy]

  def index
    resume = Resume.find(params[:resume_id])
    render json: resume.present? ? resume.basic_info : nil 
  end

  def show
    render json: @basic_info
  end

  def create
    render json: BasicInfo.create!(valid_params)
  end

  def update
    @basic_info.update_attributes!(valid_params)
    render json: @basic_info
  end

  def destroy
    render json: @basic_info.destroy
  end

  private

  def valid_params
    params.require(:basic_info).permit(BasicInfo.column_names.map {|x| x.to_sym })
  end

  def set_resource
    @basic_info = BasicInfo.find(params[:id]) unless params[:id].blank?
  end


end
