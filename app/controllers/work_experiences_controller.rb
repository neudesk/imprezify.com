class WorkExperiencesController < ApiController

  before_action :authenticate_user!
  before_action :set_resource, except: [:create, :index]

  def index
    render json: WorkExperience.with_attachments(params[:resume_id])
  end

  def create
    render json: WorkExperience.create!(valid_params)
  end

  def show
    render json: @work_experience
  end

  def update
    @work_experience.update_attributes!(valid_params)
    render json: @work_experience.reload
  end

  def destroy
    render json: @work_experience.destroy
  end

  def attachments
    render json: @work_experience.get_attachments
  end

  def upload_attachment
    render json: @work_experience.upload_attachments(params[:file])
  end

  def destroy_attachment
    render json: @work_experience.attachments.find(params[:attachment_id]).try(:destroy)
  end

  private

  def valid_params
    params.require(:work_experience).permit( WorkExperience.column_names.map { |x| x.to_sym } )
  end

  def set_resource
    @resume = current_user.resumes.find(params[:resume_id]) unless params[:resume_id].blank?
    @work_experience = WorkExperience.find(params[:id]) unless params[:id].blank?
  end

end
