class NewsletterSubscribersController < ApiController

	def create
		render json: NewsletterSubscriber.create!(email: params[:email])
	end

end
