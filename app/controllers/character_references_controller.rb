class CharacterReferencesController < ApiController

  before_action :authenticate_user!
  before_action :set_resource, only: [:index, :show, :update, :destroy]

  def index
    render json: @resume.character_references
  end

  def create
    render json: CharacterReference.create!(valid_params)
  end

  def show
    render json: @character_reference
  end

  def update
    @character_reference.update_attributes!(valid_params)
    render json: @character_reference.reload
  end

  def destroy
    render json: @character_reference.destroy
  end

  private

  def valid_params
    params.require(:character_reference).permit( CharacterReference.column_names.map { |x| x.to_sym } )
  end

  def set_resource
    @resume = current_user.resumes.find(params[:resume_id]) unless params[:resume_id].blank?
    @character_reference = CharacterReference.find(params[:id]) unless params[:id].blank?
  end

end
