class PreviewsController < ApplicationController

  rescue_from ActionController::UnknownFormat, with: :handle_unknown_format

  before_action :set_resume

  def show
    @tree = @resume.tree.as_json
    respond_to do |format|
      format.pdf do
        render pdf: "#{@resume.id}_resume_#{Time.now}",
               layout: "../previews/resume_templates/#{@resume.template_id}/layout",
               template: "previews/resume_templates/#{@resume.template_id}/index",
               show_as_html: params.key?('debug'),
               encoding: 'utf8',
               page_size: 'A4',
               viewport_size: '1280x1024',
               margin:  {   top: 5,
                            bottom: 5,
                            left: 0,
                            right: 0 }
      end
      format.html do
        render json: @tree
      end
    end
  rescue => e
    render json: e.inspect
  end

  def cover_letter
    params[:page_size] ||= 'A4'
    @tree = @resume.tree.as_json
    @template_id = params[:template_id].present? ? params[:template_id] : @resume.template_id
    template = "previews/resume_templates/#{@template_id}/cover_letter"
    respond_to do |format|
       format.pdf do
        render pdf: "#{@resume.id}_resume_#{Time.now}",
               layout: false,
               template: template,
               show_as_html: params.key?('debug'),
               encoding: 'utf8',
               page_size: 'a4',
               margin: {top: 10,
                        bottom: 10,
                        left: 10,
                        right: 10
               }
      end
      format.html do
        render template, layout: false
      end
    end

    rescue => e
    render json: e.inspect
  end

  def debug_params
    @tree = @resume.tree.as_json
    render json: @tree
  end

  def handle_unknown_format(e)
    render json: {message: e.message, errors: e.inspect}, status: 415
  end

  def generate_css
    respond_to do |format|
      format.js
    end
  end

  private

  def set_resume
    @resume = Resume.find(params[:id])
  end

end
