class VideoPitchesController < ApiController

  before_action :authenticate_user!
  before_action :set_resource, only: [:show, :update, :destroy]

  def index
    render json: current_user.get_video_piches
  end

  def show
    render json: @video
  end

  def create
    params[:video][:user_id] = current_user.id
    render json: VideoPitch.create!(valid_params)
  end

  def update
    @video.update_attributes!(valid_params)
    render json: @video
  end

  def destroy
    render json: @video.destroy
  end

  private

  def valid_params
    params.require(:video).permit!
  end

  def set_resource
    @video = VideoPitch.find(params[:id]) unless params[:id].blank?
  end

end
