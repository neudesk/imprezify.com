class ResumeConfigsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_resource, only: [:index, :show, :update, :destroy]

  def index
    render json: @config
  end

  def show
    render json: @config
  end

  def create
    render json: ResumeConfig.create!(valid_params)
  end

  def update
    @config.update_attributes!(valid_params)
    render json: @config
  end

  def destroy
    render json: @config.destroy
  end

  def fonts
    render json: { fonts: ResumeConfig::FONTS }
  end

  private

  def valid_params
    params.require(:resume_config).permit(ResumeConfig.column_names.map {|x| x.to_sym })
  end

  def set_resource
    @config = ResumeConfig.find(params[:id]) unless params[:id].blank?
    @config = ResumeConfig.where('resume_id = ? AND target = ?', params[:resume_id], params[:target]).first unless params[:resume_id].blank? || params[:target].blank?
  end

end
