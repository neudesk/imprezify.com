class ResumePageSetupsController < ApplicationController

  before_action :authenticate_user!
  before_action :set_resource, only: [:show, :update, :destroy]

  def index
    resume = Resume.find(params[:resume_id])
    render json: resume.present? ? resume.resume_page_setup : nil 
  end

  def show
  	render json: @resource
  end

  def create
    render json: ResumePageSetup.create!(valid_params)
  end

  def update
    @resource.update_attributes!(valid_params)
    render json: @resource
  end

  def destroy
    render json: @resource.destroy
  end

  private

  def valid_params
    params.require(:resume_page_setup).permit(ResumePageSetup.column_names.map {|x| x.to_sym })
  end

  def set_resource
  	@resource = ResumePageSetup.find(params[:id]) unless params[:id].blank?
  end
end
