class UsersController < ApiController

  before_action :authenticate_user!

  def myprofile
    render json: current_user
  end

end