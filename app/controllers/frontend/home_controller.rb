class Frontend::HomeController < ApplicationController

	layout false

	def index
	end

	def coming_soon
	end

	def auth
		redirect_to dashboards_path unless cookies[:auth_headers].blank?
	end

	def signup
	end

end
