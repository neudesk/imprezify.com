// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require frontend/plugins/jquery-migrate-1.1.0.min
//= require jquery_ujs
//= require jquery-ui
//= require frontend/plugins/jquery.address-1.6
//= require frontend/plugins/bootstrap.min
//= require frontend/plugins/pace
//= require frontend/plugins/parsley
//= require frontend/plugins/moment.min
//= require frontend/plugins/moment-duration-format
//= require frontend/plugins/jquery.slimscroll.min
//= require frontend/plugins/jquery.cookie
//= require angular
//= require angular-animate
//= require lodash
//= require restangular
//= require frontend/plugins/css3-animate-it
//= require frontend/plugins/videojs.record
//= require frontend/plugins/jquery.fancybox-plus
//= require frontend/plugins/jquery.flipster.min
//= require frontend/plugins/wysihtml5-0.3.0
//= require frontend/plugins/bootstrap-wysihtml5
//= require frontend/plugins/ion.rangeSlider.min
//= require frontend/plugins/bootstrap-datepicker
//= require frontend/plugins/bootstrap-colorpicker.min
//= require frontend/plugins/masked-input.min
//= require frontend/plugins/bootstrap-timepicker.min
//= require frontend/plugins/password-indicator
//= require frontend/plugins/bootstrap-combobox
//= require frontend/plugins/bootstrap-select.min
//= require frontend/plugins/form-plugins.demo.min
//= require frontend/plugins/bootstrap-tagsinput.min
//= require bootstrap-datetimepicker
//= require frontend/plugins/angular-ui-router.min
//= require frontend/plugins/typeahead.min
//= require frontend/plugins/tag-it.min
//= require frontend/plugins/daterangepicker
//= require frontend/plugins/bootstrap-datetimepicker.min
//= require frontend/plugins/bwizard.min
//= require frontend/plugins/jquery.dom-outline-1.0
//= require frontend/plugins/codemirror/codemirror
//= require frontend/plugins/codemirror/modes/css
//= require frontend/plugins/codemirror/modes/javascript
//= require frontend/plugins/jquery.plugin.min
//= require frontend/plugins/jquery.countdown
//= require frontend/plugins/login-v2.demo
//= require frontend/plugins/form-editable
//= require frontend/plugins/bootstrap-editable.min
//= require frontend/plugins/jquery.mockjax
//= require frontend/plugins/jquery.gritter.min
//= require frontend/plugins/notify
//= require frontend/plugins/angular-cookies.min
//= require frontend/plugins/angular-cookie.min
//= require frontend/plugins/select2.min
//= require frontend/plugins/angular-select2
//= require frontend/plugins/ngStorage.min
//= require frontend/plugins/ng-rateit.min
//= require frontend/plugins/ionic-range-slider
//= require frontend/plugins/ng-token-auth.min
//= require frontend/plugins/ng-file-upload.min
//= require frontend/plugins/ng-file-upload-shim.min
//= require frontend/plugins/angular-fancybox-plus
//= require frontend/plugins/angular-pdf
//= require frontend/plugins/pdfobject.min
//= require frontend/plugins/coming-soon.demo.min
//= require frontend/plugins/ui-bootstrap-tpls-2.5.0.min
//= require frontend/plugins/angular-confirm.min
//= require frontend/plugins/angular-sticky.min
//= require frontend/plugins/ng-ScrollSpy
//= require frontend/plugins/ngComboDatePicker.min 
//= require frontend/plugins/jquery-scrollspy
//= require frontend/plugins/wbn-datepicker.min
//= require frontend/plugins/apps
//= require frontend/angular/app
//= require frontend/angular/main_controller
//= require frontend/angular/dashboard/router
//= require frontend/angular/dashboard/services
//= require frontend/angular/dashboard/directives
//= require frontend/angular/dashboard/filters
//= require frontend/angular/dashboard/dashboard_controller

//= require frontend/angular/dashboard/video_pitch_controller
//= require frontend/angular/dashboard/cover_letter_controller

//= require frontend/angular/dashboard/smarteditor/smarteditor_controller
//= require frontend/angular/dashboard/smarteditor/components/basic_info_controller
//= require frontend/angular/dashboard/smarteditor/components/contact_info_controller
//= require frontend/angular/dashboard/smarteditor/components/work_experience_controller
//= require frontend/angular/dashboard/smarteditor/components/education_controller
//= require frontend/angular/dashboard/smarteditor/components/qualification_controller
//= require frontend/angular/dashboard/smarteditor/components/skill_controller
//= require frontend/angular/dashboard/smarteditor/components/character_reference_controller
//= require frontend/angular/dashboard/customize/customize_controller
