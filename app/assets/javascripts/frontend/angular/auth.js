_.contains = _.includes
var EMAIL_FORMAT = /^[a-z]+[a-z0-9._]+@[a-z]+\.[a-z.]{2,5}$/
var authentication = angular.module('authentication', ['restangular', 'ngCookies', 'ipCookie', 'ng-token-auth'])
authentication.config(function(RestangularProvider) {
    RestangularProvider.setBaseUrl(BASE_API_URL)
    RestangularProvider.setFullResponse(true)
    RestangularProvider.setErrorInterceptor(function(resp, deferred, responseHandler) {
        console.log('response', resp)
        var errors = []
        if(resp.data.errors) {
            if (resp.data.errors.full_messages) {
                errors = resp.data.errors.full_messages
            } else {
                errors = resp.data.errors
            }
            Notify.error('Error', errors.join("<br />"))
        }
    })
})

authentication.config(function($authProvider) {
    $authProvider.configure({
        apiUrl: BASE_API_URL,
        omniauthWindowType: 'sameWindow',
        authProviderPaths: {
            linkedin:   '/auth/linkedin'
        },
    });
});