var dashboard = angular.module('dashboard', ['restangular', 'ngCookies', 'ui.router', 'ngFileUpload', 'ipCookie', 'ng-token-auth',
                                              'ui.select2', 'fancyboxplus', 'ngRateIt', 'pdf', 'ngStorage', 'ion.rangeslider',
                                             'ui.bootstrap', 'angular-confirm', 'hl.sticky', 'ngScrollSpy', 'ngComboDatePicker'])
var admin = angular.module('admin', ['restangular', 'ngCookies', 'ipCookie', 'ng-token-auth'])
_.contains = _.includes

var is_customer = null
var is_supporter = null
var is_admin = null


angular.forEach([dashboard, admin], function(v, k) {
    v.run(function($rootScope, $state, ipCookie) {
        // var role = parseInt(ipCookie('role'))
        // if( ($state.current.name == 'dashboard') &&  role != 1) {
        //
        // }
    })
})
angular.forEach([dashboard, admin], function(v, k){
    v.config(function(RestangularProvider) {
        RestangularProvider.setBaseUrl(BASE_API_URL)
        RestangularProvider.setFullResponse(true)
        RestangularProvider.setErrorInterceptor(function(resp, deferred, responseHandler) {
            console.log('response', resp)
            var errors = []
            if(resp.data.errors) {
                if (resp.data.errors.full_messages) {
                    errors = resp.data.errors.full_messages
                } else {
                    errors = resp.data.errors
                }
                Notify.error('Error', errors.join("<br />"))
            }
        })
    })

    v.config(function($authProvider) {
        $authProvider.configure({
            apiUrl: BASE_API_URL
        });
    });

})
