authentication.controller('HomepageController', ['$scope', 'Restangular', 'NewsletterSubscriber', function($scope, Restangular, NewsletterSubscriber) {
	'use strict';

	$scope.newsletter_params = {}
	$scope.subscribed = false
	$scope.subscribe = function(){
		NewsletterSubscriber.create($scope.newsletter_params).then(function(res){
			if (res.id) {
				$scope.subscribed = true
			}
		})
	} 

}]);

authentication.controller('SignupController', ['$scope', '$auth', function($scope, $auth) {
    'use strict';
    $scope.email_format = EMAIL_FORMAT
    $scope.userForm = {}
    $scope.signup = function(){
        $auth.submitRegistration($scope.userForm).then(function(resp) {
            console.log('success', resp)
            Notify.success('Success!', 'Im redirecting you in 5 seconds.', 'success')
            setTimeout(function(){
                location.href = AFTER_SIGNUP
            }, 5000)
        }).catch(function(resp) {
            console.log('error', resp.data)
            if(resp.data) {
                var errors = []
                if(resp.data.errors) {
                    if (resp.data.errors.full_messages) {
                        errors = resp.data.errors.full_messages
                    } else {
                        errors = resp.data.errors
                    }
                    Notify.error('Validation Error', errors.join("\n<br />"))
                }
            }
         })
    }
}])

authentication.controller('SigninController', ['$scope', '$auth', 'ipCookie', function($scope, $auth, ipCookie) {
    'use strict';
    $scope.email_format = EMAIL_FORMAT
    $scope.userForm = {}
    $scope.signin = function(){
        $auth.submitLogin($scope.userForm).then(function(resp) {
            ipCookie('role', resp.role)
            location.href = '/dashboards'
        }).catch(function(resp) {
             Notify.error(resp.reason, resp.errors.join("\n<br />"))
        })
    }

    $scope.signInWithLinkedin = function() {
        $auth.authenticate('linkedin')
            .then(function(resp) {
                console.log('resp', resp)
            })
            .catch(function(resp) {
                Notify.error('Failed', resp)
                console.log('resp', resp)
            })
    }

    if(ipCookie('auth_headers')) {
        location.reload()
    }

}]);