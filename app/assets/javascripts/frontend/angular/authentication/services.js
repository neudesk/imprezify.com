authentication.factory('NewsletterSubscriber', ['Restangular', function (Restangular) {
  'use strict';
  var api = Restangular.all('/newsletter_subscribers');
  return {
    create: function(params){
      return api.post('', params)
    }
  }
}])

authentication.factory('User', ['Restangular', function (Restangular) {
    'use strict';
    var api = Restangular.all('auth');
    return {
        create: function(params){
            return api.post(params)
        },
        signin: function(params){
            return api.one('sign_in').post('', params)
        }
    }
}])