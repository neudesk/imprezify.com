dashboard.controller('MainController', ['$scope', '$auth', 'User', function($scope,  $auth, User) {
    'use strict';

    $scope.scrollToSection = function(section_id){
      $('html, body').animate({
          scrollTop: $("#"+section_id).offset().top - 180
      }, 800);
    }

    $scope.signout = function(){
        $auth.signOut().then(function(resp) {
            location.href = '/login'
        }).catch(function(resp) {
        })
    }

    $scope.auth = $auth.retrieveData('auth_headers')
    if($scope.auth) {
        User.myprofile().then(function (res) {
            if (res.data) {
                var current_user = res.data.plain()
                window.mySettings = {
                    email: current_user.email,
                    created_at: current_user.created_at,
                    app_id: 'dd31c5ebd6b6f9fb20ad18b9777dd460'
                }
            }
        })
    }

}])
