dashboard.directive('datepickerCustom', function () {
  'use strict';
  return {
    require: 'ngModel',
    restrict: 'A',
    scope: '=',
    link: function (scope, element, attrs) {
      var options = {
        changeMonth: true,
        changeYear: true
      };

      angular.extend(options, attrs)
      element.datepicker(options);

      scope.$on('$destroy', function () {
        if(element){
          element.datepicker('remove');
        }
      });

      scope.$watch(attrs['ngModel'], function (n,o) {
        var date = new Date(n)
        element.datepicker('update',date)
      },true);

    }
  };
});


dashboard.directive('dateSelect', function () {
    return {
        restrict: 'E',
        transclude: true,
        scope: {
            bindModel: '=ngModel',
            bindChange: '=ngChange'
        },
        template: '<input type="text" class="form-control date" data-parsley-no-focus data-parsley-group="wizard-step-1" required ng-model="bindModel" ng-change="bindChange" />',
        link: function ($scope, element, attrs) {
            element.find('.date').datetimepicker({
                format: 'YYYY/MM/DD'
            }).on('dp.change', function (selected) {
                $scope.$apply(function(){
                    $scope.bindModel = selected.date.format('YYYY/MM/DD')
                })
            })
        }
    }
})

dashboard.directive('contactSelectionField', function ($compile) {
    return {
        restrict: 'E',
        replace: true,
        templateUrl: 'angular/dashboard/directives/contact_selection.html',
        link: function ($scope, element, attrs) {

            $scope.contact_idx = attrs.contactIdx
            $scope.selected_id = attrs.selectedId
            $scope.contact_id = attrs.contactId
            $scope.count = attrs.count

            $scope.contact_lists = [
                {
                    id: 'Email',
                    text: 'Email',
                    icon: 'fa fa-envelope',
                    validation: {attr: 'data-parsley-type', value: 'email'}
                },
                {
                    id: 'Phone',
                    text: 'Phone',
                    icon: 'fa fa-phone',
                    validation: {attr: 'data-parsley-type', value: 'number'}
                },
                {
                    id: 'Mobile',
                    text: 'Mobile',
                    icon: 'fa fa-mobile',
                    validation: {attr: 'data-parsley-type', value: 'number'}
                },
                {
                    id: 'Skype',
                    text: 'Skype',
                    icon: 'fa fa-skype',
                    validation: {attr: 'data-parsley-type', value: 'required'}
                },
                {
                    id: 'Website',
                    text: 'Website',
                    icon: 'fa fa-globe',
                    validation: {attr: 'data-parsley-required', value: 'true'}
                },
                {
                    id: 'Facebook',
                    text: 'Facebook',
                    icon: 'fa fa-facebook',
                    validation: {attr: 'data-parsley-required', value: 'true'}
                },
                {
                    id: 'LinkedIn',
                    text: 'LinkedIn',
                    icon: 'fa fa-linkedin',
                    validation: {attr: 'data-parsley-required', value: 'email'}
                }
            ]

            function format(o) {
                if (o.id) {
                    var tag = $scope.contact_lists.filter(function (x) {
                        if (x.id == o.id) {
                            return x
                        }
                    })
                    if (tag.length > 0) {
                        return "<i class='" + tag[0].icon + " m-r-20' style='line-height: 30px'></i>" + o.text
                    } else {
                        return o.text
                    }
                } else {
                    return o.text
                }
            }

            element.find('.contact_selection').select2({
                placeholder: 'Select contact type',
                minimumResultsForSearch: -1,
                data: $scope.contact_lists,
                width: '100%',
                templateResult: format,
                templateSelection: format,
                escapeMarkup: function (m) {
                    return m
                }
            }).on('select2:select', function (evt) {
                if ($scope.contacts[$scope.contact_idx]) {
                    $scope.contacts[$scope.contact_idx]['name'] = evt.params.data.id
                } else {
                    $scope.contacts[$scope.contact_idx] = {name: evt.params.data.id}
                }
                $scope.$apply()
            })
            element.find('.contact_selection').val($scope.selected_id).trigger('change')
        }
    }
})

dashboard.directive('workExperienceField', function ($compile) {
    return {
        restrict: 'E',
        replace: true,
        templateUrl: 'angular/dashboard/directives/work_experience_field.html',
        link: function ($scope, element, attrs) {
            $scope.work_idx = attrs.workIdx
            $scope.count = attrs.count

            var start_date = element.find('.start_date')
            var end_date = element.find('.end_date')

            start_date.datetimepicker({
                format: 'YYYY/MM/DD'
            }).on('dp.change', function (selected) {
                $scope.work_experiences[$scope.work_idx].start_date = selected.date.format('YYYY/MM/DD')
                $scope.$apply()
                end_date.data("DateTimePicker").minDate(selected.date)
            })

            end_date.datetimepicker({
                format: 'YYYY/MM/DD'
            }).on('dp.change', function (selected) {
                $scope.work_experiences[$scope.work_idx].end_date = selected.date.format('YYYY/MM/DD')
                start_date.data("DateTimePicker").maxDate(selected.date)
            })

            element.find('.job_description').val($scope.work_experiences[$scope.work_idx].job_description)

            // element.find('.job_description').wysihtml5({
            //     html: false,
            //     link: false,
            //     image: false,
            //     size: 'xs',
            //     'font-styles': false,
            //     events: {
            //         change: function () {
            //             $scope.work_experiences[$scope.work_idx].job_description = element.find('.job_description').val()
            //             $scope.$apply()
            //         }
            //     }
            // })
        }
    }
})

dashboard.directive('qualificationField', function ($compile) {
    return {
        restrict: 'E',
        replace: true,
        templateUrl: 'angular/dashboard/directives/qualification_field.html',
        link: function ($scope, element, attrs) {
            $scope.qualification_idx = attrs.qualificationIdx
            $scope.count = attrs.count
        }
    }
})

dashboard.directive('educationField', function ($compile) {
    return {
        restrict: 'E',
        replace: true,
        transclude: true,
        templateUrl: 'angular/dashboard/directives/educations_field.html',
        link: function ($scope, element, attrs) {
            $scope.education_idx = attrs.educationIdx
            $scope.count = attrs.count

            var start_date = element.find('.start_date')
            var end_date = element.find('.end_date')

            start_date.datetimepicker({
                format: 'YYYY/MM/DD'
            }).on('dp.change', function (selected) {
                $scope.educations[$scope.education_idx].start_date = selected.date.format('YYYY/MM/DD')
                $scope.$apply()
                end_date.data("DateTimePicker").minDate(selected.date)
            })

            end_date.datetimepicker({
                format: 'YYYY/MM/DD'
            }).on('dp.change', function (selected) {
                $scope.educations[$scope.education_idx].end_date = selected.date.format('YYYY/MM/DD')
                start_date.data("DateTimePicker").maxDate(selected.date)
            })
        }
    }
})

dashboard.directive('ngIonSlider', function () {
    return {
        restrict: 'E',
        transclude: true,
        scope: {
            bindModel: '=ngModel'
        },
        template: '<input type="text" class="form-control" ng-model="bindModel" />',
        link: function ($scope, element, attrs) {
            element.ionRangeSlider({
                min: attrs.min,
                max: attrs.max,
                type: attrs.type,
                prefix: attrs.prefix + ' ',
                maxPostfix: '+',
                postfix: ' ' + attrs.postfix,
                hasGrid: false,
                from: $scope.bindModel,
                onChange: function (data) {
                    console.log('change', data.from)
                    $scope.bindModel = data.from
                    $scope.$apply()
                }
            })
        }
    }
})


dashboard.directive('ngPopoverIonSlider', function ($compile) {
    return {
        restrict: 'E',
        transclude: true,
        scope: {
            bindModel: '=ngModel'
        },
        template: '<a tabindex="0" class="btn btn-xs btn-white"><span class="glyphicon glyphicon-object-align-top"></span></a>',
        link: function ($scope, element, attrs) {
            var title = attrs.title
            element.popover({
                title: title,
                content: $compile('<ng-ion-slider ng-model="bindModel" min="' + attrs.min + '" max="' + attrs.max + '" data-type="' + attrs.type + '" postfix="' + attrs.postfix + '" />')($scope),
                html: true,
                placement: 'bottom',
                trigger: 'click'
            })
        }
    }
})

dashboard.directive('characterReferenceField', function ($compile) {
    return {
        restrict: 'E',
        replace: true,
        templateUrl: 'angular/dashboard/directives/character_reference_field.html',
        link: function ($scope, element, attrs) {
            $scope.character_reference_idx = attrs.characterReferenceIdx
            $scope.count = attrs.count
        }
    }
})

dashboard.directive('ngCssEditor', function () {
    return {
        restrict: 'E',
        transclude: true,
        scope: {
            bindModel: '=ngModel'
        },
        template: '<div ng-model="bindModel"></div>',
        link: function ($scope, element, attrs) {
            var cm = CodeMirror(element.get(0), {
                value: $scope.bindModel || 'body { } header { }',
                mode: 'css',
                lineNumbers: true,
                autoRefresh: true,
            })
            cm.refresh()
            cm.on('change', function (instance, data) {
                $scope.bindModel = instance.getValue()
            })
            window.cm = cm
        }
    }
})

dashboard.directive('ngColorPicker', function () {
    return {
        restrict: 'E',
        transclude: true,
        scope: {
            bindModel: '=ngModel'
        },
        template: '<input type="text" ng-model="bindModel" class="form-control input-sm" placeholder="{{placeholder}}" />',
        link: function ($scope, element, attrs) {
            $scope.placeholder = attrs.placeholder
            element.colorpicker({format: "hex"})
        }
    }
})

dashboard.directive('ngVideoRecorder', function ($confirm, $auth, Upload) {
    return {
        restrict: 'E',
        transclude: true,
        templateUrl: 'angular/dashboard/directives/video_recorder.html',
        link: function ($scope, element, attrs) {
            $scope.videoData = null

            $scope.discardRecording = function() {
                $scope.recorder.recorder.reset()
                $scope.videoData = null
                Notify.success('Success', 'You have discarded your recent recording, you may now record a new one.')
            }

            $scope.uploadVideo = function() {
                var unique_id = Notify.success('Uploading', "I'm uploading your recordings now, please wait for a while.", true)
                $('#page-loader').removeClass('hide')
                Upload.upload({
                    url: '/video_pitches/',
                    headers: $auth.retrieveData('auth_headers'),
                    data: {
                        video: $scope.videoData
                    }
                }).then(function (res) {
                    Notify.success('Uploaded', 'Successfully uploaded your video pitch.')
                    $scope.recorder.recorder.reset()
                    $scope.videoData = null
                    $scope.allVideos()
                    $('#page-loader').addClass('hide')
                    Notify.removeAll(unique_id)
                }, function (res) {
                    console.log('res', res)
                    Notify.error('Failed', res.data.message)
                    $('#page-loader').addClass('hide')
                    Notify.removeAll(unique_id)
                }, function (evt) {
                    console.log('in progress:', res)
                })
            }

            $scope.recorder = videojs("video",
                {
                    controls: true,
                    width: 800,
                    height: 600,
                    plugins: {
                        record: {
                            audio: true,
                            video: true,
                            maxLength: 60,
                            debug: false
                        }
                    }
                });
            $scope.recorder.on('deviceError', function () {
                Notify.error('Devise Error', player.deviceErrorCode)
            });
            $scope.recorder.on('error', function (error) {
                Notify.error('Error', error)
            });
            $scope.recorder.on('startRecord', function () {
                Notify.success('Video', 'Video is now live, you have 60 seconds.')
            });
            $scope.recorder.on('finishRecord', function () {
                console.log('video_data', $scope.recorder.recordedData)
                $scope.videoData = $scope.recorder.recordedData
                $scope.videoData['duration'] = moment.duration($scope.recorder.recorder.getDuration(), 'seconds').format("s [seconds]")
                $scope.$apply()
            });
        }
    }
})

dashboard.directive('resumeSelection', function (Resume) {
    return {
        restrict: 'E',
        replace: true,
        templateUrl: 'angular/dashboard/directives/resume_selection.html',
        link: function ($scope, element, attrs) {
            $scope.current_template = null
            $scope.template_details = []
            Resume.resume_template_details().then(function(res){
                $scope.template_details = res.data
            })
            $('#resumeTemplateModal').on('show.bs.modal', function() {
                $scope.resume.template_id = null
                element.find('.flipster').flipster()
            })

            $scope.setCurrentTemplate = function(index) {
                $scope.resume.template_id = $scope.template_details[index].template.id
            }

            $scope.updateTemplate = function() {
                Resume.update($scope.resume).then(function(res){
                    $scope.loadPDF(res.data.id, res.data.template_id)
                    $scope.loadHTML(res.data.id, res.data.template_id)
                    $('.modal').modal('hide')
                })
            }
        }
    }
})