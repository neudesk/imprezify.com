dashboard.factory('Resume', ['Restangular', function (Restangular) {
    'use strict';
    var api = Restangular.all('resumes');
    return {
        all: function(params){
            return api.getList()
        },
        create: function(params){
            return api.post(params)
        },
        find: function(id){
            return api.one('', id).get()
        },
        update: function(params){
            return api.one(params.id.toString()).patch(params)
        },
        resume_template_details: function() {
            return api.one('resume_template_details').get()
        }
    }

}])

dashboard.factory('BasicInfo', ['Restangular', function (Restangular) {
    'use strict';
    var api = Restangular.all('basic_info');
    return {
        index: function(params) {
            return api.customGET('', params)
        },
        create: function(params){
            return api.post(params)
        },
        update: function(params){
            return api.one(params.basic_info.id.toString()).patch(params)
        },
        find: function(id){
            return api.one(id.toString()).get()
        }
    }
}])

dashboard.factory('Photo', ['Restangular', function(Restangular){
    'use strict';
    var api = Restangular.all('photos')
    return {
        all: function(params){
            return api.customGET('', params)
        },
        update: function(id, params){
            return api.one(id.toString()).patch(params)
        },
        destroy: function(id){
            return api.one(id.toString()).remove()
        }
    }
}])

dashboard.factory('ContactInfo', ['Restangular', function (Restangular) {
    'use strict';
    var api = Restangular.all('contact_info');
    return {
        all: function(params){
            return api.customGET('', params)
        },
        create: function(params){
            return api.post(params)
        },
        update: function(params){
            return api.one(params.contact_info.id.toString()).patch(params)
        },
        destroy: function(id){
            return api.one(id.toString()).remove()
        },
        find: function(id){
            return api.one(id.toString()).get()
        }
    }
}])

dashboard.factory('CharacterReference', ['Restangular', function (Restangular) {
    'use strict';
    var api = Restangular.all('character_references');
    return {
        all: function(params){
            return api.customGET('', params)
        },
        create: function(params){
            return api.post(params)
        },
        update: function(params){
            return api.one(params.character_reference.id.toString()).patch(params)
        },
        destroy: function(id){
            return api.one(id.toString()).remove()
        },
        find: function(id){
            return api.one(id.toString()).get()
        }
    }
}])

dashboard.factory('WorkExperience', ['Restangular', function (Restangular) {
    'use strict';
    var api = Restangular.all('work_experiences');
    return {
        all: function(params){
            return api.customGET('', params)
        },
        create: function(params){
            return api.post(params)
        },
        update: function(params){
            return api.one(params.work_experience.id.toString()).patch(params)
        },
        destroy: function(id){
            return api.one(id.toString()).remove()
        },
        find: function(id){
            return api.one(id.toString()).get()
        },
        destroy_attachment: function(params){
            return api.one(params.id.toString()).one('destroy_attachment').customPOST(params)
        }
    }
}])

dashboard.factory('Qualification', ['Restangular', function (Restangular) {
    'use strict';
    var api = Restangular.all('qualifications');
    return {
        all: function(params){
            return api.customGET('', params)
        },
        create: function(params){
            return api.post(params)
        },
        update: function(params){
            return api.one(params.qualification.id.toString()).patch(params)
        },
        destroy: function(id){
            return api.one(id.toString()).remove()
        },
        find: function(id){
            return api.one(id.toString()).get()
        },
        destroy_attachment: function(params){
            return api.one(params.id.toString()).one('destroy_attachment').customPOST(params)
        }
    }
}])

dashboard.factory('Skill', ['Restangular', function (Restangular) {
    'use strict';
    var api = Restangular.all('skills');
    return {
        all: function(params){
            return api.customGET('', params)
        },
        create: function(params){
            return api.post(params)
        },
        update: function(params){
            return api.one(params.skill.id.toString()).patch(params)
        },
        destroy: function(id){
            return api.one(id.toString()).remove()
        },
        find: function(id){
            return api.one(id.toString()).get()
        }
    }
}])

dashboard.factory('Education', ['Restangular', function (Restangular) {
    'use strict';
    var api = Restangular.all('educations');
    return {
        all: function(params){
            return api.customGET('', params)
        },
        create: function(params){
            return api.post(params)
        },
        update: function(params){
            return api.one(params.education.id.toString()).patch(params)
        },
        destroy: function(id){
            return api.one(id.toString()).remove()
        },
        find: function(id){
            return api.one(id.toString()).get()
        },
        destroy_attachment: function(params){
            return api.one(params.id.toString()).one('destroy_attachment').customPOST(params)
        }
    }
}])

dashboard.factory('ResumeConfig', ['Restangular', function (Restangular) {
    'use strict';
    var api = Restangular.all('resume_configs');
    return {
        all: function(params){
            return api.customGET('', params)
        },
        create: function(params){
            return api.post(params)
        },
        update: function(params){
            return api.one(params.id.toString()).patch(params)
        },
        destroy: function(id){
            return api.one(id.toString()).remove()
        },
        find: function(params){
            return api.one(params.id.toString())
        },
        fonts: function(){
            return api.one('fonts').get()
        }
    }
}])

dashboard.factory('Preview', ['Restangular', function (Restangular) {
    'use strict';
    var api = Restangular.all('previews');
    return {
        pdf: function(params) {
            return '/previews/'+params.resume_id.toString()+'?format=pdf&template_id='+params.template_id
        },
        html: function(params){
            return '/previews/'+params.resume_id.toString()+'?debug=true&format=pdf&template_id='+params.template_id
        }
    }
}])


dashboard.factory('ResumePageSetup', ['Restangular', function (Restangular) {
    'use strict';
    var api = Restangular.all('resume_page_setups');
    return {
        index: function(params) {
            return api.customGET('', params)
        },
        create: function(params){
            return api.post(params)
        },
        update: function(params){
            return api.one(params.resume_page_setup.id.toString()).patch(params)
        },
        find: function(id){
            return api.one(id.toString()).get()
        }
    }
}])

dashboard.factory('CoverLetter', ['Restangular', function (Restangular) {
    'use strict';
    var api = Restangular.all('cover_letters');
    return {
        all: function(params) {
            return api.customGET('', params)
        },
        create: function(params){
            return api.post(params)
        },
        update: function(params){
            return api.one(params.cover_letter.id.toString()).patch(params)
        },
        find: function(id){
            return api.one(id.toString()).get()
        },
        destroy: function(id){
            return api.one(id.toString()).remove()
        },
    }
}])

dashboard.factory('VideoPitch', ['Restangular', function (Restangular) {
    'use strict';
    var api = Restangular.all('video_pitches');
    return {
        all: function(params){
            return api.customGET('', params)
        },
        find: function(id){
            return api.one(id.toString()).get()
        },
        update: function(params){
            return api.one(params.video.id.toString()).patch(params)
        },
        destroy: function(id){
            return api.one(id.toString()).remove()
        }
    }
}])

dashboard.factory('User', ['Restangular', function (Restangular) {
    'use strict';
    var api = Restangular.all('users');
    return {
        myprofile: function(){
            return api.one('myprofile').get()
        }
    }
}])