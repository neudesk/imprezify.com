// dashboard routers
dashboard.config(function ($stateProvider, $urlRouterProvider) {
    $stateProvider.state('dashboard', {
        url: '/',
        templateUrl: '/angular/dashboard/resumes.html',
        controller: 'DashboardController',
        resolve: {
            auth: function ($auth) {
                return $auth.validateUser().catch(function () {
                    location.href = '/login'
                })
            }
        }
    }).state('smarteditor', {
        url: '/smarteditor/:id',
        templateUrl: '/angular/dashboard/smarteditor/smarteditor.html',
        controller: 'SmartEditorController',
        resolve: {
            auth: function ($auth) {
                return $auth.validateUser().catch(function () {
                    location.href = '/login'
                })
            }
        }
    }).state('customize', {
        url: '/customize/:id',
        templateUrl: '/angular/dashboard/customize/customize.html',
        controller: 'CustomizeController',
        resolve: {
            auth: function ($auth) {
                return $auth.validateUser().catch(function () {
                    location.href = '/login'
                })
            }
        }
    }).state('pitches', {
        url: '/pitches/:id',
        templateUrl: '/angular/dashboard/video_pitches.html',
        controller: 'VideoPitchController',
        resolve: {
            auth: function ($auth) {
                return $auth.validateUser().catch(function () {
                    location.href = '/login'
                })
            }
        }
    }).state('letters', {
        url: '/letters/:id',
        templateUrl: '/angular/dashboard/letters.html',
        controller: 'CoverLetterController',
        resolve: {
            auth: function ($auth) {
                return $auth.validateUser().catch(function () {
                    location.href = '/login'
                })
            }
        }
    })
    $urlRouterProvider.otherwise('/')
})