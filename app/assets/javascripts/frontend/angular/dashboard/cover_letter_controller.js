dashboard.controller('CoverLetterController', ['$scope', '$rootScope', '$state', '$confirm', 'CoverLetter', 'Resume', function ($scope, $rootScope, $state, $confirm, CoverLetter, Resume) {
    'use strict';
    $rootScope.controller = 'dashboard'
    $rootScope.action = 'letters'
    $scope.save_method = 0 //0 for create 1 for update


    //get all cover letters
    $scope.cover_letters = []
    $scope.getAllCoverLetter = function(){
    	CoverLetter.all()
    	.then(function(res){
    			res = res.data ? res.data.plain() : res.plain()
    			$scope.cover_letters = res
    	})
    }

    $scope.cover_letter = {}
    $scope.preCreateCoverLetter = function(){
    	$scope.save_method = 0
    	$scope.cover_letter = {}
    	$('#create_cover_letter').modal('show')
    }

    $scope.createCoverLetter = function(){
    	CoverLetter.create($scope.cover_letter).then(function(res){
    		$scope.getAllCoverLetter()
        $scope.cover_letter = {}
        Notify.success('Created', 'Cover Letter successfully created')
        $('.modal').modal('hide')
    	})
    }

    $scope.saveCoverLetter = function(){
    	if($scope.save_method == 0){
    		$scope.createCoverLetter()
    	}else{
    		$scope.updateCoverLetter()
    	}
    }

    $scope.updateCoverLetter = function(){
    	CoverLetter.update({cover_letter: $scope.cover_letter}).then(function(res){
        $('.modal').modal('hide')
        Notify.success('Updated', 'Cover Letter successfully updated')
        $scope.getAllCoverLetter()
    	})
    }

    $scope.selectCoverLetter = function(id){
    	$scope.save_method = 1
    	CoverLetter.find(id).then(function(res){
    		res = res.data ? res.data.plain() : res.plain()
    		$scope.cover_letter = res
    		$('#create_cover_letter').modal('show')
    	})
    }

    $scope.deleteCoverLetter = function(id){
      $confirm({text: 'Are you sure you want to delete?'}, {size: 'sm'}).then(function() {
        CoverLetter.destroy(id).then(function(res){
	    		Notify.success('Deleted', 'Cover Letter successfully deleted')
	    		$scope.getAllCoverLetter()
	    	})
      });
    	
    }

    //get all resumes
    $scope.resumes = []
    function getAllResumes(){
    	Resume.all().then(function(res){
          res = res.data ? res.data.plain() : res.plain()
          console.log(res)
          $scope.resumes = res
      })
    }


    // init
    // $rootScope.controller = $state.current.name.split('.')[0]
    if($state.current.name == 'letters') {
        $rootScope.action = 'letters'
        $scope.getAllCoverLetter()
        getAllResumes()
    }
}])