dashboard.controller('VideoPitchController', ['$scope', '$rootScope', '$confirm', 'VideoPitch', function ($scope, $rootScope, $confirm, VideoPitch) {
    'use strict';
    $rootScope.controller = 'dashboard'
    $rootScope.action = 'pitches'

    $scope.player = videojs('player')

    $scope.videos = []
    $scope.allVideos = function() {
        VideoPitch.all({}).then(function(res){
            if(res.data) {
                $scope.videos = res.data
                if(res.data.length) {
                    $scope.getVideo(0)
                }
            }
        })
    }

    $scope.video = null
    $scope.getVideo = function(index) {
        $scope.video = $scope.videos[index]
        $scope.player.src(
            [
                { type: "video/mp4", src: $scope.video.mp4 },
                { type: "video/webm", src: $scope.video.webm }
            ]
        )
    }

    $scope.setAsPrimary = function(index) {
        var video = $scope.videos[index]
        $confirm({text: 'Are you sure you want to set this video as primary?'}, {size: 'sm'}).then(function() {
            VideoPitch.update({video: {id: video.id, is_primary: true}}).then(function() {
                Notify.success('Success', 'You have successfuly set ' + video.video_file_name + ' as primary.')
                $scope.allVideos()
            })
        })
    }

    $scope.deleteVideo = function(index) {
        var video = $scope.videos[index]
        $confirm({text: 'Are you sure you want to delete this video?'}, {size: 'sm'}).then(function() {
            VideoPitch.destroy(video.id).then(function() {
                Notify.success('Success', 'You have successfuly deleted ' + video.video_file_name + '.')
                $scope.allVideos()
            })
        })
    }

    // init
    $scope.allVideos()

}])