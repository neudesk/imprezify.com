dashboard.controller('DashboardController', ['$scope', '$rootScope', '$state', 'Resume', 'User',
                                             function($scope, $rootScope, $state, Resume, User) {
    'use strict';

    // list all resumes
    $scope.resumes = []
    $scope.getAllResumes = function(){
        Resume.all().then(function(res){
            $scope.resumes = res.data.plain()
            console.log(res)
        })
    }

    // create resume
    $scope.resume = {}
    $scope.createResume = function(){
        Resume.create({resume: $scope.resume}).then(function(res){
            $scope.getAllResumes()
            $('.modal').modal('hide')
            $scope.resume = {}
            $('.modal').on('hidden.bs.modal', function(){
                $state.go('editor', { id: res.data.plain().id })
            })
        })
    }

    // init
    $rootScope.controller = $state.current.name.split('.')[0]
    if($state.current.name == 'dashboard') {
        $rootScope.action = 'resumes'
        $scope.getAllResumes()
    }
}])