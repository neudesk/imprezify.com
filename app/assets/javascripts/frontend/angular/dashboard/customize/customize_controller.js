dashboard.controller('CustomizeController', ['$scope', '$state', '$sce', 'Preview', 'Resume', 'ResumeConfig',
    function ($scope, $state, $sce, Preview, Resume, ResumeConfig) {
    'use strict';

    // resume
    $scope.resume = {id: null}
    $scope.getResume = function () {
        Resume.find($state.params.id).then(function (res) {
            if (res.data) {
                $scope.resume = res.data
                $scope.loadHTML()
            }
        })
    }

    $scope.loadHTML = function () {
        $('#html_preview').attr('data', Preview.html({resume_id: $state.params.id, template_id: $scope.resume.id}))
    }

    // Resume Config
    $scope.staticConfig = {}
    $scope.config = {}
    $scope.fonts = []
    $scope.font_styles = ['normal', 'italic', 'oblique', 'initial', 'inherit']
    $scope.getFonts = function (params) {
        ResumeConfig.fonts().then(function (res) {
            $scope.fonts = res.data.plain().fonts
        })
    }

    $scope.getConfig = function () {
        ResumeConfig.all({ resume_id: $state.params.id, target: $scope.selectedDom }).then(function (res) {
            if (res.data == undefined) {
                $scope.createConfig({})
                return
            }
            $scope.config = res.data.plain()
            $scope.staticConfig = angular.copy($scope.config)
        })
    }

    $scope.createConfig = function (params) {
        params.resume_id = $state.params.id
        params.target = $scope.selectedDom
        ResumeConfig.create(params).then(function(){
            $scope.getConfig()
        })
    }

    $scope.updateConfig = function (params) {
        if(params.configs) {
            params.configs = JSON.stringify(params.configs)
        }
        ResumeConfig.update(params).then(function(){
            $scope.getConfig()
            $scope.loadHTML($scope.resume.template_id)
            $('.modal').modal('hide')
        })
    }

    $scope.saveConfig = function() {
        $scope.updateConfig($scope.config)
    }

    $scope.resetConfig = function() {
        $scope.config['margin_top'] = null
        $scope.config['margin_right'] = null
        $scope.config['margin_bottom'] = null
        $scope.config['margin_left'] = null
        $scope.config['padding_top'] = null
        $scope.config['padding_right'] = null
        $scope.config['padding_bottom'] = null
        $scope.config['padding_left'] = null
        $scope.config['text_override'] = null
        $scope.config['font_family'] = null
        $scope.config['font_size'] = null
        $scope.config['font_style'] = null
        $scope.config['font_color'] = null
        $scope.saveConfig()
        $scope.getConfig()
        Notify.success('Success', 'Styles has been restored to defaults.')
    }

    // resume customization

    $scope.selectedDom = atob(window.localStorage.getItem('selectedDom'))
    $scope.selectedElem = $sce.trustAsHtml(atob(window.localStorage.getItem('selectedElem')))
    $scope.getSelectedDom = function() {
        setInterval(function() {
            $scope.selectedDom = atob(window.localStorage.getItem('selectedDom'))
            $scope.$digest()
        }, 1000)
    }

    $scope.$watch(function() { return $scope.selectedDom } ,function(newVal,oldVal){
        if(newVal != oldVal){
            $scope.getConfig()
            $('#configModal').modal('show')
        }
    })
    $scope.getSelectedDom()

    $scope.tabs = [
        { title:'Content Overide'},
        { title:'Fonts'},
        { title:'Margins'},
    ];

    // init
    $scope.getResume()
}])