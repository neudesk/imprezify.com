dashboard.controller('SmartEditorController', ['$scope', '$state', function ($scope, $state) {
    'use strict';
    $scope.resume_id = $state.params.id

    $scope.smarteditor_menu_offset = $('#header').innerHeight()

    $scope.toCustomize = function(){
    	$('#smarteditor').parsley().validate()
    	Notify.error('Failed', 'Please Complete the Form to continue')
    }
}])