dashboard.controller('QualificationController',
    ['$scope', '$state', '$auth', '$confirm', 'Qualification', 'Upload',
    function ($scope, $state, $auth, $confirm, Qualification, Upload) {
    'use strict';

// qualifications
      $scope.qualifications = []

      $scope.addQualificationFields = function () {
          $scope.saveQualification()
          $scope.createQualification({})
      }

      $scope.getQualifications = function (params) {
          Qualification.all({resume_id: $state.params.id}).then(function (res) {
              res.data = res.data.plain()
              if (res.data.length === 0) {
                  $scope.createQualification({resume_id: $state.params.id})
                  return
              }
              $scope.qualifications = res.data
          })
      }

      $scope.uploadQualicationSupportingDocs = function (files, qualification_id, qualificationIdx) {
        if(files.length > 0) {
          Upload.upload({
              url: '/qualifications/' + qualification_id + '/upload_attachment',
              headers: $auth.retrieveData('auth_headers'),
              data: {
                  file: files
              }
          }).then(function (res) {
              $scope.qualifications[qualificationIdx].attachments = res.data
              Notify.success('Uploaded', 'Successfully uploaded ' + _.last(res.data).data_file_name)
          }, function (res) {
              Notify.error('Failed', 'Unable to upload, make sure your file is an image file. Please try again.')
          }, function (evt) {
              console.log('in progress:', res)
          })
        }
      }

      $scope.createQualification = function (qualification) {
          qualification.resume_id = $state.params.id
          Qualification.create({qualification: qualification}).then(function() {
              $scope.getQualifications()
          })
      }

      $scope.updateQualification = function (qualification) {
          Qualification.update({qualification: qualification}).then(function(){
              $scope.getQualifications()
          })
      }

      $scope.deleteQualification = function (qualification_idx, qualification_id) {
          $confirm({text: 'Are you sure you want to delete?'}, {size: 'sm'}).then(function() {
              if (qualification_id) {
                Qualification.destroy(qualification_id).then(function(){
                  $scope.getQualifications()
                })
              }
          });
      }

      $scope.deleteQualificationAttachment = function (qualification_idx, fileIdx) {
          var qualification = $scope.qualifications[qualification_idx]
          var attachment = qualification.attachments[fileIdx]
          $confirm({text: 'Are you sure you want to delete?'}, {size: 'sm'}).then(function() {
             Qualification.destroy_attachment({
                  id: qualification.id.toString(),
                  attachment_id: attachment.id.toString()
              }).then(function (res) {
                  qualification.attachments.splice(fileIdx, 1)
                  $scope.tmp = qualification.attachments
                  qualification.attachments = []
                  qualification.attachments = $scope.tmp
                  Notify.success('Success', 'Successfully deleted ' + attachment.data_file_name)
              })
          });
      }

      $scope.saveQualification = function () {
          angular.forEach($scope.qualifications, function (qualification, key) {
              console.log('qualification', qualification)
              if (qualification.id) {
                  $scope.updateQualification(qualification)
              }
          })
      }

  // init
  $scope.getQualifications()

}])