dashboard.controller('EducationController',
    ['$scope', '$state', '$auth', '$confirm', 'Education', 'Upload',
    function ($scope, $state, $auth, $confirm, Education, Upload) {
    'use strict';

    // Education
      $scope.degree_levels = ['Primary', 'Secondary', 'Degree', 'Masteral', 'Doctorates', 
                              'Associates', 'Vocational', 'Undergraduate']
      $scope.educations = []

      $scope.createEducation = function (education) {
          education.resume_id = $state.params.id
          Education.create({education: education}).then(function (res) {
              $scope.getEducations()
          })
      }

      $scope.getEducations = function () { 
        Education.all({resume_id: $state.params.id}).then(function (res) {
          res.data = res.data.plain()
          if (res.data.length === 0) {
              $scope.createEducation({resume_id: $state.params.id})
              return
          }
          angular.forEach(res.data, function (education, idx) {
              if (education.start_date) {
                  education.start_date = moment(education.start_date).format('YYYY/MM/DD')
              }
              if (education.end_date) {
                  education.end_date = moment(education.end_date).format('YYYY/MM/DD')
              }
          })
          $scope.educations = res.data
        })
      }

      $scope.updateEducation = function (education) {
          Education.update({education: education}).then(function(){
            $scope.getEducations()
          })
      }

      $scope.deleteEducation = function (education_idx, education_id) {
          $confirm({text: 'Are you sure you want to delete?'}, {size: 'sm'}).then(function() {
              if (education_id) {
                  Education.destroy(education_id).then(function(){
                    $scope.getEducations()
                  })
              }  
          });
      }

      $scope.uploadEducationSupportingDocs = function (files, education_id, education_idx) {
         if(files.length > 0) {
            Upload.upload({
              url: '/educations/' + education_id + '/upload_attachment',
              headers: $auth.retrieveData('auth_headers'),
              data: {
                  file: files
              }
            }).then(function (res) {
              $scope.educations[education_idx].attachments = res.data
              console.log('uploaded', _.last(res.data))
              Notify.success('Uploaded', 'Successfully uploaded ' + _.last(res.data).data_file_name)
            }, function (res) {
              Notify.error('Failed', 'Unable to upload, make sure your file is an image file. Please try again.')
            }, function (evt) {
              console.log('in progress:', res)
            })
         }
      }

      $scope.deleteEducationAttachment = function (education_idx, fileIdx) {
          var education = $scope.educations[education_idx]
          var attachment = education.attachments[fileIdx]
          $confirm({text: 'Are you sure you want to delete?'}, {size: 'sm'}).then(function() {
            Education.destroy_attachment({
                  id: education.id.toString(),
                  attachment_id: attachment.id.toString()
              }).then(function (res) {
                  education.attachments.splice(fileIdx, 1)
                  $scope.tmp = education.attachments
                  education.attachments = []
                  education.attachments = $scope.tmp
                  Notify.success('Success', 'Successfully deleted ' + attachment.data_file_name)
              })
          });
          
      }

      $scope.saveEducation = function () {
          angular.forEach($scope.educations, function (education, key) {
              if (education.id) {
                  $scope.updateEducation(education)
              }
          })
      }

      $scope.addEducationFields = function () {
        $scope.saveEducation()
        $scope.createEducation({})
      }

    // init
    $scope.getEducations()

}])