dashboard.controller('CharacterReferenceController',
    ['$scope', '$state', '$auth', '$confirm', 'CharacterReference',
        function ($scope, $state, $auth, $confirm, CharacterReference) {
            'use strict';

    // character references
    $scope.character_references = []

    $scope.addCharReferenceFields = function () {
        $scope.saveCharReference()
        $scope.createCharReference({})
    }

    $scope.getCharReferences = function () {
        CharacterReference.all({resume_id: $state.params.id}).then(function (res) {
            res.data = res.data.plain()
            if (res.data.length == 0) {
                $scope.createCharReference({})
                return
            }
            $scope.character_references = res.data
        })
    }

    $scope.createCharReference = function (character_reference) {
        character_reference.resume_id = $state.params.id
        CharacterReference.create({character_reference: character_reference}).then(function() {
            $scope.getCharReferences()
        })
    }

    $scope.saveCharReference = function () {
        angular.forEach($scope.character_references, function (character_reference, key) {
            if (character_reference.id) {
                $scope.updateCharReference(character_reference)
            }
        })
    }

    $scope.updateCharReference = function (character_reference) {
        character_reference.resume_id = $state.params.id
        CharacterReference.update({character_reference: character_reference}).then(function(){
            $scope.getCharReferences()
        })
    }

    $scope.deleteCharReference = function (character_reference_idx, character_reference_id) {
        $confirm({text: 'Are you sure you want to delete?'}, {size: 'sm'}).then(function() {
            if (character_reference_id) {
                CharacterReference.destroy(character_reference_id).then(function(){
                    $scope.getCharReferences()
                })
            }
        });
    }

    // init
    $scope.getCharReferences()

}])