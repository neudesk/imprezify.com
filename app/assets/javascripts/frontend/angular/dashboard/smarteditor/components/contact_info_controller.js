dashboard.controller('ContactInfoController',
    ['$scope', '$state', '$confirm', 'ContactInfo',
    function ($scope, $state, $confirm, ContactInfo) {
    'use strict';

    // contacts
    $scope.contacts = []
    $scope.addContactFields = function () {
        $scope.saveContacts()
        $scope.createContactInfo({})
    }

    $scope.getContactInfos = function () {
        ContactInfo.all({resume_id: $state.params.id}).then(function (res) {
            res.data = res.data.plain()
            if (res.data.length === 0) {
                $scope.createContactInfo({})
            }
            $scope.contacts = res.data
        })
    }

    $scope.createContactInfo = function (contact) {
        contact.resume_id = $state.params.id
        ContactInfo.create({contact_info: contact}).then(function() {
            $scope.getContactInfos()
        })
    }

    $scope.updateContactInfo = function (contact) {
        ContactInfo.update({contact_info: contact}).then(function(){
            $scope.getContactInfos()
        })
    }

    $scope.tmp = null
    $scope.deleteContact = function (contact_idx, contact_id) {
        if (contact_id) {
            $confirm({text: 'Are you sure you want to delete?'}, {size: 'sm'}).then(function() {
                ContactInfo.destroy(contact_id).then(function(){
                    $scope.getContactInfos()
                })
            });
        }
    }

    $scope.saveContacts = function () {
        angular.forEach($scope.contacts, function (contact, key) {
            if (contact.id) {
                $scope.updateContactInfo(contact)
            }
        })
    }

    // init
    $scope.getContactInfos()

}])