dashboard.controller('SkillController',
    ['$scope', '$state', '$auth', '$confirm', 'Skill', 'Upload',
    function ($scope, $state, $auth, $confirm, Skill, Upload) {
    'use strict';

    // skills

    $scope.skills = []

    $scope.getSkills = function () {
      Skill.all({resume_id: $state.params.id}).then(function (res) {
          res.data = res.data.plain()
          if (res.data.length === 0) {
              $scope.createSkill({})
              return
          }
          $scope.skills = res.data
      })
    }

    $scope.createSkill = function (skill) {
      skill.resume_id = $state.params.id
      Skill.create({skill: skill}).then(function (res) {
          $scope.getSkills()
      })
    }

    $scope.updateSkill = function (skill) {
      skill.resume_id = $state.params.id
      Skill.update({skill: skill})
    }

    $scope.deleteSkill = function (skill_idx, skill_id) {
      $confirm({text: 'Are you sure you want to delete?'}, {size: 'sm'}).then(function() {
          if (skill_id) {
              Skill.destroy(skill_id).then(function(){
                $scope.getSkills()
              })
          }
      });
    }

    $scope.addSkillFields = function () {
      $scope.saveSkills()
      $scope.createSkill({})
    }

    $scope.saveSkills = function () {
      angular.forEach($scope.skills, function (skill, key) {
          if (skill.id) {
              $scope.updateSkill(skill)
          }
      })
    }

    //init
    $scope.getSkills()

}])