dashboard.controller('BasicInfoController',
    ['$scope', '$state', '$auth', 'BasicInfo', 'Photo', 'Resume', 'Upload',
    function ($scope, $state, $auth, BasicInfo, Photo, Resume, Upload) {
    'use strict';

    // resume
    $scope.resume = {id: null}
    $scope.getResume = function () {
        Resume.find($state.params.id).then(function (res) {
            if (res.data) {
                $scope.resume = res.data
            }
        })
    }

    //basic infos
    $scope.basic_info = {resume_id: $state.params.id, birthday: new Date()}
    $scope.$watch('basic_info.birthday', function(newVal, oldVal){
        $scope.basic_info.age = moment().diff(newVal, 'years');
    })
    $scope.getBasicInfo = function () {
        BasicInfo.index({resume_id: $state.params.id}).then(function (res) {
            $scope.basic_info_is_loaded = true
            if (res.data) {
                $scope.basic_info = res.data
                $scope.basic_info.birthday = res.data.birthday
            }
        })
    }

    $scope.createBasicInfo = function () {
        BasicInfo.create({basic_info: $scope.basic_info}).then(function (res) {
            $scope.basic_info = res.data
        })
    }

    $scope.updateBasicInfo = function () {
        BasicInfo.update({basic_info: $scope.basic_info}).then(function (res) {
            $scope.basic_info = res.data
            $scope.getBasicInfo()
        })
    }

    $scope.saveBasicInfo = function () {
        if ($scope.basic_info.id) {
            $scope.updateBasicInfo($scope.basic_info)
        }
    }

    //basic infos for primary photo
    $scope.user_photos = []
    $scope.togglePhotoTool = function(index, show){
        $scope.user_photos[index].show_tool = show
    }

    //get all photos
    $scope.primary_photo = null
    $scope.getResumePhotos = function(){
        Photo.all().then(function(res){
            $scope.user_photos = res.data
        })
    }

    $scope.showUserPhotosModal = function(){
        $('#user-photos-modal').modal('show')
    }

    $scope.deletePhoto = function(index, id){
        $confirm({text: 'Are you sure you want to delete?'}, {size: 'sm'}).then(function() {
            Photo.destroy(id).then(function(res){
                Notify.success('Deleted', 'Photo successfully deleted')
                $scope.getResumePhotos()
            })
        });

    }

    $scope.setPrimaryPhoto = function(index, id){
        Photo.update(id, {photo: {resume_id:  $scope.resume.id}} ).then(function(res){
            Notify.success('Primary Photo', 'Photo successfully set')
            $scope.getResume()
        })
    }

    $scope.uploadUserPhotos = function (files, user_id) {
        if(files.length) {
            Upload.upload({
                url: '/photos',
                headers: $auth.retrieveData('auth_headers'),
                data: {
                    user_id: $auth.retrieveData('user_id'),
                    photo: files
                }
            }).then(function (res) {
                $scope.getResumePhotos()
                Notify.success('Uploaded', 'Successfully uploaded ' + _.last(res.data).data_file_name)
            }, function (res) {
                Notify.error('Failed', 'Unable to upload, make sure your file is an image file. Please try again.')
            }, function (evt) {
                console.log('in progress:', res)
            })
        }
    }

    $scope.$watch('basic_info.birthday', function(newVal, oldVal){
        $scope.basic_info.age = moment().diff(newVal, 'years');
    })

    // init
    $scope.getResume()
    $scope.getBasicInfo()
    $scope.getResumePhotos()

}])