dashboard.controller('WorkExperienceController',
    ['$scope', '$state', '$auth', '$confirm', 'WorkExperience', 'Upload',
        function ($scope, $state, $auth, $confirm, WorkExperience, Upload) {
            'use strict';

            // work experience
            $scope.work_experiences = [{
                id: null,
                work_title: null,
                company: null,
                start_date: null,
                end_date: null,
                current: false,
                job_description: null,
                resume_id: null
            }]

            $scope.addWorkExperienceFields = function () {
                $scope.saveWorkExperience()
                $scope.createWorkExperience({})
            }

            $scope.getWorkExperiences = function () {
                WorkExperience.all({resume_id: $state.params.id}).then(function (res) {
                    res.data = res.data.plain()
                    if (res.data.length === 0) {
                        $scope.createWorkExperience({resume_id: $state.params.id})
                        return
                    }
                    angular.forEach(res.data, function (work, idx) {
                        if (work.start_date) {
                            work.start_date = moment(work.start_date).format('YYYY/MM/DD')
                        }
                        if (work.end_date) {
                            work.end_date = moment(work.end_date).format('YYYY/MM/DD')
                        }
                    })
                    $scope.work_experiences = res.data
                })
            }

            $scope.uploadWorkExperienceSupportingDocs = function (files, work_experience_id, workIdx) {
                Upload.upload({
                    url: '/work_experiences/' + work_experience_id + '/upload_attachment',
                    headers: $auth.retrieveData('auth_headers'),
                    data: {
                        file: files
                    }
                }).then(function (res) {
                    $scope.work_experiences[workIdx].attachments = res.data
                    console.log('uploaded', _.last(res.data))
                    Notify.success('Uploaded', 'Successfully uploaded ' + _.last(res.data).data_file_name)
                }, function (res) {
                    Notify.error('Failed', 'Unable to upload, make sure your file is an image file. Please try again.')
                }, function (evt) {
                    console.log('in progress:', res)
                })
            }

            $scope.createWorkExperience = function (work) {
                work.resume_id = $state.params.id
                WorkExperience.create({work_experience: work}).then(function() {
                    $scope.getWorkExperiences()
                })
            }

            $scope.updateWorkExperience = function (work) {
                WorkExperience.update({work_experience: work}).then(function() {
                    $scope.getWorkExperiences()
                })
            }

            $scope.deleteWorkExperience = function (work_idx, work_id) {
                $confirm({text: 'Are you sure you want to delete?'}, {size: 'sm'}).then(function() {
                    if (work_id) {
                        WorkExperience.destroy(work_id).then(function(){
                            $scope.getWorkExperiences()
                        })
                    }
                });
            }

            $scope.deleteWorkExperienceAttachment = function (work_idx, fileIdx) {
                var work = $scope.work_experiences[work_idx]
                var attachment = work.attachments[fileIdx]
                $confirm({text: 'Are you sure you want to delete?'}, {size: 'sm'}).then(function() {
                    WorkExperience.destroy_attachment({
                        id: work.id.toString(),
                        attachment_id: attachment.id.toString()
                    }).then(function (res) {
                        work.attachments.splice(fileIdx, 1)
                        $scope.tmp = work.attachments
                        work.attachments = []
                        work.attachments = $scope.tmp
                        Notify.success('Success', 'Successfully deleted ' + attachment.data_file_name)
                    })
                });

            }

            $scope.saveWorkExperience = function () {
                angular.forEach($scope.work_experiences, function (work_experience, key) {
                    if (work_experience.id) {
                        $scope.updateWorkExperience(work_experience)
                    }
                })
            }

            // init
            $scope.getWorkExperiences()

        }
    ]
)