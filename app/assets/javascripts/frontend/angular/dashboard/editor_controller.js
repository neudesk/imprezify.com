dashboard.controller('EditorController', ['$window', '$sce', '$scope', '$localStorage', '$rootScope', '$state', '$timeout', '$auth', '$confirm', 'Upload', 'Resume', 'BasicInfo', 'ContactInfo', 'CharacterReference', 'WorkExperience', 'Qualification', 'Skill', 'Education', 'Preview', 'ResumeConfig', 'Photo', 'ResumePageSetup',
  function ($window, $sce, $scope, $localStorage, $rootScope, $state, $timeout, $auth, $confirm, Upload, Resume, BasicInfo, ContactInfo, CharacterReference, WorkExperience, Qualification, Skill, Education, Preview, ResumeConfig, Photo, ResumePageSetup) {
      'use strict';

      // resume
      $scope.resume = {id: null}
      $scope.getResume = function () {
          Resume.find($state.params.id).then(function (res) {
              if (res.data) {
                  $scope.resume = res.data
              }
          })
      }
     
      //basic infos
      $scope.basic_info_is_loaded = false
      $scope.basic_info = {resume_id: $state.params.id}
      $scope.getBasicInfo = function () {
          $scope.basic_info_is_loaded = false
          BasicInfo.index({resume_id: $state.params.id}).then(function (res) {
              $scope.basic_info_is_loaded = true
              if (res.data) {
                  res.data.birthday = moment(res.data.birthday).format('DD/MM/YYYY')
                  $scope.basic_info = res.data
              }
          })
      }

      $scope.createBasicInfo = function () {
          BasicInfo.create({basic_info: $scope.basic_info}).then(function (res) {
              $scope.basic_info = res.data
          })
      }

      $scope.updateBasicInfo = function () {
          BasicInfo.update({basic_info: $scope.basic_info}).then(function (res) {
              $scope.basic_info = res.data
          })
      }

      $scope.saveBasicInfo = function () {
          if ($scope.basic_info.id) {
              $scope.updateBasicInfo($scope.basic_info)
          } else {
              $scope.createBasicInfo($scope.basic_info)
          }
      }

      //basic infos for primary photo
      $scope.user_photos = []
      $scope.togglePhotoTool = function(index, show){
          $scope.user_photos[index].show_tool = show
      }

      //get all photos
      $scope.getUserPhotos = function(){
          Photo.all().then(function(res){
              res = res.data ? res.data.plain() : res.plain()
              $scope.user_photos = res
          })
      }


      $scope.showUserPhotosModal = function(){
          $('#user-photos-modal').modal('show')
          $scope.getUserPhotos()
      }

      $scope.deletePhoto = function(index, id){
          $confirm({text: 'Are you sure you want to delete?'}, {size: 'sm'}).then(function() {
             Photo.destroy(id).then(function(res){
                  console.log(res)
                  Notify.success('Deleted', 'Photo successfully deleted')
                  $scope.user_photos.splice(index,1)
              })
          });
         
      }

      $scope.setPrimaryPhoto = function(index, id){
          Photo.update(id, {photo: {resume_id:  $scope.resume.id}} ).then(function(res){
              res = res.data? res.data.plain() : res.plain()
              Notify.success('Primary Photo', 'Photo successfully set')
              $scope.resume.photo_primary = res
          })
      }

      $scope.uploadUserPhotos = function (files, user_id) {
          if(files.length < 1 || !user_id) return
          Upload.upload({
              url: '/photos',
              headers: $auth.retrieveData('auth_headers'),
              data: {
                  user_id: $auth.retrieveData('user_id'),
                  photo: files
              }
          }).then(function (res) {
              $scope.user_photos = res.data
              console.log('uploaded', _.last(res.data))
              Notify.success('Uploaded', 'Successfully uploaded ' + _.last(res.data).data_file_name)
          }, function (res) {
              Notify.error('Failed', 'Unable to upload, make sure your file is an image file. Please try again.')
          }, function (evt) {
              console.log('in progress:', res)
          })
      }


      // contacts
      $scope.contacts = []

      $scope.addContactFields = function () {
          $scope.saveContacts()
          $scope.createContactInfo({})
      }

      $scope.getContactInfos = function () {
          ContactInfo.all({resume_id: $state.params.id}).then(function (res) {
              res.data = res.data.plain()
              if (res.data.length === 0) {
                  $scope.createContactInfo({})
              }
              $scope.contacts = res.data
          })
      }

      $scope.createContactInfo = function (contact) {
          contact.resume_id = $scope.resume.id
          ContactInfo.create({contact_info: contact}).then(function() {
              $scope.getContactInfos()
          })
      }

      $scope.updateContactInfo = function (contact) {
          contact.resume_id = $scope.resume.id
          ContactInfo.update({contact_info: contact}).then(function(){
              $scope.getContactInfos()
          })
      }

      $scope.tmp = null
      $scope.deleteContact = function (contact_idx, contact_id) {
          if (contact_id) {
              $confirm({text: 'Are you sure you want to delete?'}, {size: 'sm'}).then(function() {
                  ContactInfo.destroy(contact_id).then(function(){
                      $scope.getContactInfos()
                  })
              });
          }
      }



      $scope.saveContacts = function () {
          angular.forEach($scope.contacts, function (contact, key) {
              if (contact.id) {
                  $scope.updateContactInfo(contact)
              }
          })
      }

      // character references
      $scope.character_references = []

      $scope.addCharReferenceFields = function () {
          $scope.saveCharReference()
          $scope.createCharReference({})
      }

      $scope.getCharReferences = function () {
          CharacterReference.all({resume_id: $scope.resume.id}).then(function (res) {
              res.data = res.data.plain()
              if (res.data.length == 0) {
                  $scope.createCharReference({resume_id: $scope.resume.id})
                  return
              }
              $scope.character_references = res.data
          })
      }

      $scope.createCharReference = function (character_reference) {
          character_reference.resume_id = $scope.resume.id
          CharacterReference.create({character_reference: character_reference}).then(function() {
              $scope.getCharReferences()
          })
      }

      $scope.saveCharReference = function () {
          console.log($scope.character_references)
          angular.forEach($scope.character_references, function (character_reference, key) {
              if (character_reference.id) {
                  $scope.updateCharReference(character_reference)
              }
          })
      }

      $scope.updateCharReference = function (character_reference) {
          character_reference.resume_id = $scope.resume.id
          CharacterReference.update({character_reference: character_reference}).then(function(){
              $scope.getCharReferences()
          })
      }

      $scope.deleteCharReference = function (character_reference_idx, character_reference_id) {
          $confirm({text: 'Are you sure you want to delete?'}, {size: 'sm'}).then(function() {
              if (character_reference_id) {
                  CharacterReference.destroy(character_reference_id).then(function(){
                      $scope.getCharReferences()
                  })
              }    
          });
      }


      // work experience
      $scope.work_experiences = [{
          id: null,
          work_title: null,
          company: null,
          start_date: null,
          end_date: null,
          current: false,
          job_description: null,
          resume_id: null
      }]

      $scope.addWorkExperienceFields = function () {
          $scope.saveWorkExperience()
          $scope.createWorkExperience({})
      }

      $scope.getWorkExperiences = function () {
          WorkExperience.all({resume_id: $scope.resume.id}).then(function (res) {
              res.data = res.data.plain()
              if (res.data.length === 0) {
                  $scope.createWorkExperience({resume_id: $scope.resume.id})
                  return
              }
              angular.forEach(res.data, function (work, idx) {
                  if (work.start_date) {
                      work.start_date = moment(work.start_date).format('YYYY/MM/DD')
                  }
                  if (work.end_date) {
                      work.end_date = moment(work.end_date).format('YYYY/MM/DD')
                  }
              })
              $scope.work_experiences = res.data
          })
      }

      $scope.uploadWorkExperienceSupportingDocs = function (files, work_experience_id, workIdx) {
          Upload.upload({
              url: '/work_experiences/' + work_experience_id + '/upload_attachment',
              data: {
                  file: files
              }
          }).then(function (res) {
              $scope.work_experiences[workIdx].attachments = res.data
              console.log('uploaded', _.last(res.data))
              Notify.success('Uploaded', 'Successfully uploaded ' + _.last(res.data).data_file_name)
          }, function (res) {
              Notify.error('Failed', 'Unable to upload, make sure your file is an image file. Please try again.')
          }, function (evt) {
              console.log('in progress:', res)
          })
      }

      $scope.createWorkExperience = function (work) {
          work.resume_id = $scope.resume.id
          WorkExperience.create({work_experience: work}).then(function() {
              $scope.getWorkExperiences()
          })
      }

      $scope.updateWorkExperience = function (work) {
          work.resume_id = $scope.resume.id
          WorkExperience.update({work_experience: work}).then(function() {
              $scope.getWorkExperiences()
          })
      }

      $scope.deleteWorkExperience = function (work_idx, work_id) {
          $confirm({text: 'Are you sure you want to delete?'}, {size: 'sm'}).then(function() {
              if (work_id) {
                  WorkExperience.destroy(work_id).then(function(){
                      $scope.getWorkExperiences()
                  })
              } 
          });
      }

      $scope.deleteWorkExperienceAttachment = function (work_idx, fileIdx) {
          var work = $scope.work_experiences[work_idx]
          var attachment = work.attachments[fileIdx]
          $confirm({text: 'Are you sure you want to delete?'}, {size: 'sm'}).then(function() {
              WorkExperience.destroy_attachment({
                  id: work.id.toString(),
                  attachment_id: attachment.id.toString()
              }).then(function (res) {
                  work.attachments.splice(fileIdx, 1)
                  $scope.tmp = work.attachments
                  work.attachments = []
                  work.attachments = $scope.tmp
                  Notify.success('Success', 'Successfully deleted ' + attachment.data_file_name)
              })
          });
          
      }

      $scope.saveWorkExperience = function () {
          angular.forEach($scope.work_experiences, function (work_experience, key) {
              if (work_experience.id) {
                  $scope.updateWorkExperience(work_experience)
              }
          })
      }

      // qualifications
      $scope.qualifications = [{id: null, description: null, resume_id: null}]

      $scope.addQualificationFields = function () {
          $scope.saveQualification()
          $scope.createQualification({})
      }

      $scope.getQualifications = function (params) {
          Qualification.all({resume_id: $scope.resume.id}).then(function (res) {
              res.data = res.data.plain()
              if (res.data.length === 0) {
                  $scope.createQualification({resume_id: $scope.resume.id})
                  return
              }
              $scope.qualifications = res.data
          })
      }

      $scope.uploadQualicationSupportingDocs = function (files, qualification_id, qualificationIdx) {
          console.log('qualification_id', qualification_id)
          Upload.upload({
              url: '/qualifications/' + qualification_id + '/upload_attachment',
              data: {
                  file: files
              }
          }).then(function (res) {
              $scope.qualifications[qualificationIdx].attachments = res.data
              console.log('uploaded', _.last(res.data))
              Notify.success('Uploaded', 'Successfully uploaded ' + _.last(res.data).data_file_name)
          }, function (res) {
              Notify.error('Failed', 'Unable to upload, make sure your file is an image file. Please try again.')
          }, function (evt) {
              console.log('in progress:', res)
          })
      }

      $scope.createQualification = function (qualification) {
          qualification.resume_id = $scope.resume.id
          Qualification.create({qualification: qualification}).then(function() {
              $scope.getQualifications({resume_id: $scope.resume.id})
          })
      }

      $scope.updateQualification = function (qualification) {
          qualification.resume_id = $scope.resume.id
          Qualification.update({qualification: qualification}).then(function(){
              $scope.getQualifications()
          })
      }

      $scope.deleteQualification = function (qualification_idx, qualification_id) {
          $confirm({text: 'Are you sure you want to delete?'}, {size: 'sm'}).then(function() {
              $scope.qualifications.splice(qualification_idx, 1)
              $scope.tmp = angular.copy($scope.qualifications)
              $scope.qualifications = []
              $scope.qualifications = $scope.tmp
              if (qualification_id) {
                Qualification.destroy(qualification_id)
              }
          });
      }

      $scope.deleteQualificationAttachment = function (qualification_idx, fileIdx) {
          var qualification = $scope.qualifications[qualification_idx]
          var attachment = qualification.attachments[fileIdx]
          $confirm({text: 'Are you sure you want to delete?'}, {size: 'sm'}).then(function() {
             Qualification.destroy_attachment({
                  id: qualification.id.toString(),
                  attachment_id: attachment.id.toString()
              }).then(function (res) {
                  qualification.attachments.splice(fileIdx, 1)
                  $scope.tmp = qualification.attachments
                  qualification.attachments = []
                  qualification.attachments = $scope.tmp
                  Notify.success('Success', 'Successfully deleted ' + attachment.data_file_name)
              })
          });
      }

      $scope.saveQualification = function () {
          angular.forEach($scope.qualifications, function (qualification, key) {
              if (qualification.id) {
                  $scope.updateQualification(qualification)
              }
          })
      }



      // skills

      $scope.skills = []

      $scope.getSkills = function (params) {
          Skill.all(params).then(function (res) {
              res.data = res.data.plain()
              if (res.data.length === 0) {
                  $scope.createSkill({resume_id: $scope.resume.id})
                  return
              }
              $scope.skills = res.data
          })
      }

      $scope.createSkill = function (skill) {
          skill.resume_id = $scope.resume.id
          Skill.create({skill: skill}).then(function (res) {
              $scope.getSkills({resume_id: $scope.resume.id})
          })
      }

      $scope.updateSkill = function (skill) {
          skill.resume_id = $scope.resume.id
          Skill.update({skill: skill})
      }

      $scope.deleteSkill = function (skill_idx, skill_id) {
          $confirm({text: 'Are you sure you want to delete?'}, {size: 'sm'}).then(function() {
             $scope.skills.splice(skill_idx, 1)
              $scope.tmp = angular.copy($scope.skills)
              $scope.skills = []
              $scope.skills = $scope.tmp
              if (skill_id) {
                  Skill.destroy(skill_id)
              }
          });
      }

      $scope.addSkillFields = function () {
          $scope.createSkill({})
          $scope.saveSkills()
      }

      $scope.saveSkills = function () {
          angular.forEach($scope.skills, function (skill, key) {
              if (skill.id) {
                  $scope.updateSkill(skill)
              } else {
                  if (skill.name) {
                      $scope.createSkill(skill)
                  }
              }
          })
      }

      // Education
      $scope.degree_levels = ['Primary', 'Secondary', 'Degree', 'Masteral',
          'Doctorates', 'Associates', 'Vocational', 'Undergraduate']
      $scope.educations = []

      $scope.createEducation = function (education) {
          education.resume_id = $scope.resume.id
          Education.create({education: education}).then(function (res) {
              $scope.getEducations({resume_id: $scope.resume.id})
          })
      }

      $scope.getEducations = function (params) {
          Education.all(params).then(function (res) {
              res.data = res.data.plain()
              if (res.data.length === 0) {
                  $scope.createEducation({resume_id: $scope.resume.id})
                  return
              }
              angular.forEach(res.data, function (education, idx) {
                  if (education.start_date) {
                      education.start_date = moment(education.start_date).format('YYYY/MM/DD')
                  }
                  if (education.end_date) {
                      education.end_date = moment(education.end_date).format('YYYY/MM/DD')
                  }
              })
              $scope.educations = res.data
          })
      }

      $scope.updateEducation = function (education) {
          education.resume_id = $scope.resume.id
          Education.update({education: education})
      }

      $scope.deleteEducation = function (education_idx, education_id) {
          $confirm({text: 'Are you sure you want to delete?'}, {size: 'sm'}).then(function() {
              $scope.educations.splice(education_idx, 1)
              $scope.tmp = angular.copy($scope.educations)
              $scope.educations = []
              $scope.educations = $scope.tmp
              if (education_id) {
                  Education.destroy(education_id)
              }  
          });
      }

      $scope.uploadEducationSupportingDocs = function (files, education_id, education_idx) {
          Upload.upload({
              url: '/educations/' + education_id + '/upload_attachment',
              data: {
                  file: files
              }
          }).then(function (res) {
              $scope.educations[education_idx].attachments = res.data
              console.log('uploaded', _.last(res.data))
              Notify.success('Uploaded', 'Successfully uploaded ' + _.last(res.data).data_file_name)
          }, function (res) {
              Notify.error('Failed', 'Unable to upload, make sure your file is an image file. Please try again.')
          }, function (evt) {
              console.log('in progress:', res)
          })
      }

      $scope.deleteEducationAttachment = function (education_idx, fileIdx) {
          var education = $scope.educations[education_idx]
          var attachment = education.attachments[fileIdx]
          $confirm({text: 'Are you sure you want to delete?'}, {size: 'sm'}).then(function() {
            Education.destroy_attachment({
                  id: education.id.toString(),
                  attachment_id: attachment.id.toString()
              }).then(function (res) {
                  education.attachments.splice(fileIdx, 1)
                  $scope.tmp = education.attachments
                  education.attachments = []
                  education.attachments = $scope.tmp
                  Notify.success('Success', 'Successfully deleted ' + attachment.data_file_name)
              })
          });
          
      }

      $scope.saveEducation = function () {
          angular.forEach($scope.educations, function (education, key) {
              if (education.id) {
                  $scope.updateEducation(education)
              } else {
                  if (education.name) {
                      $scope.createEducation(education)
                  }
              }
          })
      }

      $scope.addEducationFields = function () {
          $scope.createEducation({})
          $scope.saveEducation()
      }

       // resume page setup
      $scope.resume_page_setup = {page_size_type: '', orientation: 'portrait', width: 0, height: 0, margin_top: 0, margin_right: 0, margin_bottom: 0, margin_left: 0}
      
      // page sizes are depending on the DPI 
      // 72 DPI
      $scope.page_sizes = [
        {name: 'letter', label: 'letter (8.5" x 11")', width: 612, height: 792},
        {name: 'tabloid', label: 'tabloid (11" x 17") ', width: 792, height: 1224},
        {name: 'legal', label: 'legal (8.5" x 14")', width: 612, height: 1008},
        {name: 'statement', label: 'statement (5.5" x 8.5")', width: 396, height: 612},
        {name: 'executive', label: 'executive (7.25" x 10.5")', width: 522, height: 756},
        {name: 'folio', label: 'folio (8.5" x 13")', width: 612, height: 936},
        {name: 'a3', label: 'a3 (11.69" x 16.54")', width: 842, height: 1191},
        {name: 'a4', label: 'a4 (8.27" x 11.69")', width: 595, height: 842},
        {name: 'a5', label: 'a5 (5.83" x 8.27")', width: 420, height: 595},
        {name: 'b4', label: 'b4 (9.84" x 13.90")', width: 708, height: 1001},
        {name: 'b5', label: 'b5 (6.93" x 9.84")', width: 499, height: 708},
        {name: 'custom', label: 'custom', width: 0, height: 0}
      ]

      $scope.openResumePageSetupModal = function(){
        $('#resumePageSetupModal').modal('show')
      }

      $scope.setResumePageSize = function(){
        var page_size = {}
        angular.forEach($scope.page_sizes, function(v, k){
          if(v.name == $scope.resume_page_setup.page_size_type){
            page_size = $scope.page_sizes[k]
          }
        })
        $scope.resume_page_setup.width = page_size.width
        $scope.resume_page_setup.height = page_size.height
      }

      $scope.getResumePageSetup = function(){
        ResumePageSetup.index({resume_id: $state.params.id}).then(function (res) {
            if (res.data) {
              console.log(res)
              $scope.resume_page_setup = res.data
            }
        })
      }

      $scope.createResumePageSetup = function(params){
        params['resume_id'] =  $state.params.id
         ResumePageSetup.create({resume_page_setup: params}).then(function (res) {
            $scope.resume_page_setup = res.data
          })
      }

      $scope.updateResumePageSetup = function(params){
         ResumePageSetup.update({resume_page_setup: params}).then(function (res) {
              $scope.resume_page_setup = res.data
              Notify.success('Success', 'Page Setup update success.')
          })
      }


      $scope.saveResumePageSetup = function () {
          if ($scope.resume_page_setup.id) {
              $scope.updateResumePageSetup($scope.resume_page_setup)
          } else {
              $scope.createResumePageSetup($scope.resume_page_setup)
          }
          $('#resumePageSetupModal').modal('hide')
      }



      // PDF preview config

      $scope.pdfOptions = {
          height: "1123px",
          pdfOpenParams: {
              view: 'FitV',
              navpanes: 0,
              toolbar: 0,
              statusbar: 0
          }
      }

      $scope.loadPDF = function (resume_id, template_id) {
          PDFObject.embed(Preview.pdf({
              resume_id: resume_id,
              template_id: template_id
          }), "#preview", $scope.pdfOptions)
      }

      $scope.loadHTML = function (resume_id, template_id) {
          $('#html_preview').attr('data', Preview.html({resume_id: resume_id, template_id: template_id}))
      }

      // Resume Config
      $scope.staticConfig = {}
      $scope.config = {}
      $scope.fonts = []
      $scope.font_styles = ['normal', 'italic', 'oblique', 'initial', 'inherit']
      $scope.getFonts = function (params) {
          ResumeConfig.fonts().then(function (res) {
              $scope.fonts = res.data.plain().fonts
          })
      }

      $scope.getConfig = function () {
          ResumeConfig.all({ resume_id: $scope.resume.id, target: $scope.selectedDom }).then(function (res) {
              if (res.data == undefined) {
                  $scope.createConfig({})
                  return
              }
              $scope.config = res.data.plain()
              $scope.staticConfig = angular.copy($scope.config)
          })
      }

      $scope.createConfig = function (params) {
          params.resume_id = $scope.resume.id
          params.target = $scope.selectedDom
          ResumeConfig.create(params).then(function(){
              $scope.getConfig()
          })
      }

      $scope.updateConfig = function (params) {
          if(params.configs) {
              params.configs = JSON.stringify(params.configs)
          }
          ResumeConfig.update(params).then(function(){
              $scope.getConfig()
              $scope.loadPDF($scope.resume.id, $scope.resume.template_id)
              $scope.loadHTML($scope.resume.id, $scope.resume.template_id)
              Notify.success('Success', 'Styles has been saved.')
              $('.modal').modal('hide')
          })
      }

      $scope.saveConfig = function() {
          console.log('config', $scope.config)
          $scope.updateConfig($scope.config)
      }

      $scope.resetConfig = function() {
          $scope.config['margin_top'] = null
          $scope.config['margin_right'] = null
          $scope.config['margin_bottom'] = null
          $scope.config['margin_left'] = null
          $scope.config['padding_top'] = null
          $scope.config['padding_right'] = null
          $scope.config['padding_bottom'] = null
          $scope.config['padding_left'] = null
          $scope.config['text_override'] = null
          $scope.config['font_family'] = null
          $scope.config['font_size'] = null
          $scope.config['font_style'] = null
          $scope.config['font_color'] = null
          $scope.saveConfig()
          $scope.getConfig()
          Notify.success('Success', 'Styles has been restored to defaults.')
      }

      // init
      $scope.handleBootstrapWizardsValidation = function () {
          "use strict";
          $("#wizard").bwizard({
              validating: function (e, ui) {
                  console.log('ui_index', ui.index)
                  if (ui.index == 0) {
                      // step-1 validation
                      if($scope.basic_info_is_loaded) {
                          if (false === $('form[name="form-wizard"]').parsley().validate('wizard-step-1')) {
                              Notify.error('Validation Failed', 'Validation failed, Please check all fields.')
                              return false;
                          }
                      }
                  } else if (ui.index == 1) {
                      // step-2 validation
                      if (false === $('form[name="form-wizard"]').parsley().validate('wizard-step-2')) {
                          Notify.error('Validation Failed', 'Validation failed, Please check all fields.')
                          return false;
                      }
                  } else if (ui.index == 2) {
                      // step-3 validation
                      if (false === $('form[name="form-wizard"]').parsley().validate('wizard-step-3')) {
                          Notify.error('Validation Failed', 'Validation failed, Please check all fields.')
                          return false;
                      }
                  } else if (ui.index == 3) {
                      // step-4 validation
                      if (false === $('form[name="form-wizard"]').parsley().validate('wizard-step-4')) {
                          Notify.error('Validation Failed', 'Validation failed, Please check all fields.')
                          return false;
                      }
                  } else if (ui.index == 4) {
                      // step-4 validation
                      if (false === $('form[name="form-wizard"]').parsley().validate('wizard-step-5')) {
                          Notify.error('Validation Failed', 'Validation failed, Please check all fields.')
                          return false;
                      }
                  }
              },
              activeIndexChanged: function (e, ui) {
                  console.log('index', ui.index)
                  switch (ui.index) {
                      case 0:
                          $rootScope.action = ui.index
                          $scope.getBasicInfo()
                          $scope.getContactInfos()
                      case 1:
                          $rootScope.action = ui.index
                          $scope.saveBasicInfo()
                          $scope.saveContacts()
                          $scope.getCharReferences()
                          $scope.getWorkExperiences()
                          break;
                      case 2:
                          $rootScope.action = ui.index
                          $scope.saveWorkExperience()
                          $scope.saveCharReference()
                          $scope.getQualifications({resume_id: $scope.resume.id})
                          $scope.getSkills({resume_id: $scope.resume.id})
                          break;
                      case 3:
                          $rootScope.action = ui.index
                          $scope.saveQualification()
                          $scope.saveSkills()
                          $scope.getEducations({resume_id: $scope.resume.id})
                          break;
                      case 4:
                          // skills
                          $rootScope.action = ui.index
                          // qualification
                          $scope.saveEducation()
                          $scope.loadPDF($scope.resume.id, $scope.resume.template_id)
                          $scope.loadHTML($scope.resume.id, $scope.resume.template_id)
                          $scope.getFonts()
                          // resume page setup
                          $scope.getResumePageSetup()
                          break;
                      default:
                          alert('not implemented')
                  }
              }
          });
      };

      $scope.refreshCSSEditor = function () {
          setTimeout($window.cm.refresh(), 300)
      }

      // init resume
      $scope.getResume()
      $scope.getBasicInfo()
      $scope.getContactInfos()

      $timeout(function () {
          $rootScope.controller = $state.current.name.split('.')[0]
          if ($state.current.name == 'editor') {
              $rootScope.action = 0
              $('li.editor a').trigger('click')
          }
      })

      $scope.fancyboxOptions = {
          scrolling: false
      }


      // resume customization

      $scope.selectedDom = atob(window.localStorage.getItem('selectedDom'))
      $scope.selectedElem = $sce.trustAsHtml(atob(window.localStorage.getItem('selectedElem')))
      $scope.getSelectedDom = function() {
          setInterval(function() {
              $scope.selectedDom = atob(window.localStorage.getItem('selectedDom'))
              $scope.$digest()
          }, 1000)
      }

      $scope.$watch(function() { return $scope.selectedDom } ,function(newVal,oldVal){
         if(newVal != oldVal){
              $scope.getConfig()
              $('#configModal').modal('show')
          }
      })
      $scope.getSelectedDom()

      $scope.tabs = [
          { title:'Content Overide'},
          { title:'Fonts'},
          { title:'Margins'},
      ];
  }])