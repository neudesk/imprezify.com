var Notify = {

    error: function(title, message, sticky) {
        this.set_content(title, message)
        this.options.image = '/angular/images/error-icon.png'
        this.options.class_name = 'notify_error'
        this.options.sticky = sticky ? true : false
        return this.grit()
    },

    success: function(title, message, sticky) {
        this.set_content(title, message)
        this.options.image = '/angular/images/success-icon.png'
        this.options.class_name = 'notify_success'
        this.options.sticky = sticky ? true : false
        return this.grit()
    },

    set_content: function(title, message) {
        this.options.title = title
        this.options.text = message
        this.options.message = message
    },

    grit: function() {
        return $.gritter.add(this.options)
    },

    removeAll: function(id) {
        $.gritter.remove(id)
    },

    options: {
        title: '',
        text: '',
        image: '',
        sticky: false,
        time: 8000,
        class_name: ''
    }

}