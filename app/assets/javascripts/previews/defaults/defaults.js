$(document).ready(function () {
    $(".skill-rating").each(function (k, elem) {
        $(elem).rateYo({
            numStars: 5,
            rating: $(elem).data('rate'),
            starWidth: '12px',
            fullStart: true,
            spacing: '3px',
            diabled: true
        })
    })

    var domSelector = {}
    var onClickHandler = function(element) {
        localStorage.setItem('selectedDom', btoa(fullPath(element)))
        domSelector.start()
    }
    var domSelector = DomOutline({ onClick: onClickHandler });
    domSelector.start();
})

function fullPath(el){
  var names = [];
  while (el.parentNode){
    if (el.id){
      names.unshift('#'+el.id);
      break;
    }else{
      if (el==el.ownerDocument.documentElement) names.unshift(el.tagName);
      else{
        for (var c=1,e=el;e.previousElementSibling;e=e.previousElementSibling,c++);
        names.unshift(el.tagName+":nth-child("+c+")");
      }
      el=el.parentNode;
    }
  }
  return names.join(" > ");
}